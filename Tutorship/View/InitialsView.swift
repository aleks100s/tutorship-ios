//
//  InitialsView.swift
//  Tutorship
//
//  Created by Alexander on 25.05.2021.
//

import UIKit

class InitialsView: UIView {
	@IBOutlet var contentView: UIView!
	@IBOutlet weak var roundView: UIView!
	@IBOutlet weak var initialsLabel: UILabel!
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupView()
	}
	
	required init?(coder: NSCoder) {
		super.init(coder: coder)
		setupView()
	}
	
	func setupView() {
		Bundle.main.loadNibNamed("InitialsView", owner: self, options: nil)
		addSubview(contentView)
		contentView.frame = self.bounds
		contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
		
		let radius = roundView.frame.size.height / 2
		roundView.layer.cornerRadius = radius
		initialsLabel.text = ""
	}
	
	override func layoutSubviews() {
		let radius = roundView.frame.size.height / 2
		roundView.layer.cornerRadius = radius
		initialsLabel.font = UIFont.systemFont(ofSize: radius > 25 ? radius * 0.8 : radius * 0.55)
	}
}
