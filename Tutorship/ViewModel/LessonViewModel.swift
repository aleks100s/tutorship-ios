//
//  LessonViewModel.swift
//  Tutorship
//
//  Created by Alexander on 06.06.2021.
//

import UIKit

class LessonViewModel {
	static func loadTodayLessons(completion: @escaping ([LessonViewModel]) -> Void) {
		let now = Double(Int64(Date().timeIntervalSince1970)) * 1000
		let calendar = Calendar.current
		let tomorrow = Date(timeIntervalSinceNow: 24 * 60 * 60)
		let finish = calendar.startOfDay(for: tomorrow).timeIntervalSince1970 * 1000
		Repository.shared.getLessons(dateFrom: now, dateTo: finish) { lessons in
			let viewModels = lessons.map { lesson in
				LessonViewModel(lesson: lesson)
			}
			completion(viewModels)
		}
	}
	
	static func loadNextLessons(completion: @escaping ([LessonViewModel]) -> Void) {
		let calendar = Calendar.current
		let tomorrow = Date(timeIntervalSinceNow: 24 * 60 * 60)
		let tomorrowMillis = calendar.startOfDay(for: tomorrow).timeIntervalSince1970 * 1000
		Repository.shared.getLessons(after: tomorrowMillis) { lessons in
			let viewModels = lessons.map { lesson in
				LessonViewModel(lesson: lesson)
			}
			completion(viewModels)
		}
	}
	
	static func create(datetime: Date, duration: Double, price: Int, groupID: Int, topic: String, completion: @escaping () -> Void) {
		let lesson = Lesson(datetime: datetime, duration: duration, price: price, groupID: groupID, topic: topic, comment: "")
		Repository.shared.createLesson(lesson: lesson) {
			completion()
		}
	}
	
	static func createMultipleLessons(groupID: Int, topics: String, price: Int, startDate: Date, duration: Double, endDate: Date, scheduleType: ScheduleType, weekdays: [Weekday] = [], completion: @escaping () -> Void) {
		var calendar = Calendar.current
		calendar.locale = Locale(identifier: "ru_RU")
		calendar.firstWeekday = 2
		
		var date = startDate
		switch scheduleType {
		case .everyday:
			while calendar.startOfDay(for: date) != calendar.startOfDay(for: endDate) {
				create(datetime: date, duration: duration, price: price, groupID: groupID, topic: topics) {}
				date = calendar.date(byAdding: .day, value: 1, to: date) ?? Date()
			}
		case .everyWeek:
			let numbers = weekdays.map({$0.getNumber()})
			while calendar.startOfDay(for: date) != calendar.startOfDay(for: endDate) {
				if numbers.contains(calendar.component(.weekday, from: date)) {
					create(datetime: date, duration: duration, price: price, groupID: groupID, topic: topics) {}
				}
				date = calendar.date(byAdding: .day, value: 1, to: date) ?? Date()
			}
		case .workDays:
			while calendar.startOfDay(for: date) != calendar.startOfDay(for: endDate) {
				if !calendar.isDateInWeekend(date) {
					create(datetime: date, duration: duration, price: price, groupID: groupID, topic: topics) {}
				}
				date = calendar.date(byAdding: .day, value: 1, to: date) ?? Date()
			}
		}
		completion()
	}
	
	static func loadLessons(from: Date, to: Date, completion: @escaping ([LessonViewModel]) -> Void) {
		let calendar = Calendar.current
		let fromMillis = calendar.startOfDay(for: from).timeIntervalSince1970 * 1000
		let startOfNextMonth = calendar.date(byAdding: .day, value: 1, to: to) ?? Date()
		let toMillis = calendar.startOfDay(for: startOfNextMonth).timeIntervalSince1970 * 1000
		Repository.shared.getLessons(dateFrom: fromMillis, dateTo: toMillis) { lessons in
			let viewModels = lessons.map { lesson in
				LessonViewModel(lesson: lesson)
			}
			completion(viewModels)
		}
	}
	
	let lessonID: LiveData<Int> = LiveData(0)
	let groupID: LiveData<Int> = LiveData(0)
	let lessonDate: LiveData<String> = LiveData("")
	let lessonTimeStart: LiveData<String> = LiveData("")
	let lessonTimeEnd: LiveData<String> = LiveData("")
	let lessonPrice: LiveData<String> = LiveData("")
	let lessonPriceInt: LiveData<Int> = LiveData(0)
	let lessonTopic: LiveData<String> = LiveData("")
	let lessonDuration: LiveData<Int> = LiveData(0)
	let lessonComment: LiveData<String> = LiveData("")
	let lessonFinished: LiveData<Bool> = LiveData(false)
	let groupName: LiveData<String> = LiveData("")
	let subjectID: LiveData<Int> = LiveData(0)
	let groupSubject: LiveData<String> = LiveData("")
	let groupComment: LiveData<String> = LiveData("")
	let groupColor: LiveData<UIColor> = LiveData(UIColor.clear)
	let lessonStartDate: LiveData<Date> = LiveData(Date())
	var dateMillis: Double = 0
	
	var fullDateFormatter: DateFormatter {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "EEE, dd.MM"
		dateFormatter.locale = Locale(identifier: "ru_RU")
		return dateFormatter
	}
	
	var hoursDateFormatter: DateFormatter {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "HH:mm"
		return dateFormatter
	}
	
	init() {}
	
	init(lesson: FullLesson) {
		setup(with: lesson)
	}
	
	private func setup(with lesson: FullLesson) {
		self.lessonID.value = lesson.lesson?.id ?? 0
		self.groupID.value = lesson.group?.id ?? 0
		self.lessonDate.value = fullDateFormatter.string(from: lesson.lesson?.datetime ?? Date())
		self.lessonTimeStart.value = hoursDateFormatter.string(from: lesson.lesson?.datetime ?? Date())
		self.lessonTimeEnd.value = hoursDateFormatter.string(from: Date(timeIntervalSince1970: (lesson.lesson?.datetime.timeIntervalSince1970 ?? 0) + (lesson.lesson?.duration ?? 0)))
		self.lessonDuration.value = Int((lesson.lesson?.duration ?? 0) / (60 * 1000))
		self.lessonPrice.value = "\(lesson.lesson?.price ?? 0)₽"
		self.lessonPriceInt.value = lesson.lesson?.price ?? 0
		self.lessonTopic.value = lesson.lesson?.topic ?? ""
		self.lessonComment.value = lesson.lesson?.comment ?? ""
		self.lessonFinished.value = lesson.lesson?.isFinished ?? false
		self.groupName.value = lesson.group?.name ?? ""
		self.subjectID.value = lesson.group?.subjectID ?? 0
		self.groupSubject.value = lesson.group?.subject ?? ""
		self.groupComment.value = lesson.group?.comment ?? ""
		self.groupColor.value = UIColor(hex: lesson.group?.color ?? "#FFFFFF") ?? .white
		let date = lesson.lesson?.datetime ?? Date()
		let startOfDay = Calendar.current.startOfDay(for: date)
		self.dateMillis = startOfDay.timeIntervalSince1970
		self.lessonStartDate.value = lesson.lesson?.datetime ?? Date()
	}
	
	func deleteLesson(completion: @escaping () -> Void) {
		Repository.shared.deleteLesson(id: self.lessonID.value) {
			completion()
		}
	}
	
 }
