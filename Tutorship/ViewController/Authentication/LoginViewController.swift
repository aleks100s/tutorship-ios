//
//  LoginViewController.swift
//  Tutorship
//
//  Created by Alexander on 22.05.2021.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class LoginViewController: BaseViewController {
	override class var storyboardName: String { "Authentication" }
	
	@IBOutlet weak var emailTextField: MDCOutlinedTextField!
	@IBOutlet weak var passwordTextField: MDCOutlinedTextField!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		prepareViews()
    }
	
	private func prepareViews() {
		UIHelper.prepareTextFields(textFields: [emailTextField, passwordTextField])
		emailTextField.label.text = "E-Mail"
		passwordTextField.label.text = "Пароль"
		
		emailTextField.delegate = self
		passwordTextField.delegate = self
	}
	
	@IBAction func loginPressed(_ sender: Any) {
		let email = emailTextField.text ?? ""
		let password = passwordTextField.text ?? ""
		
		if !InputValidator.emailValid(email) {
			UIHelper.showErrorInField(emailTextField, text: "Проверьте введенныe данныe")
			return
		}
		
		if !InputValidator.passwordValid(password) {
			UIHelper.showErrorInField(passwordTextField, text: "Проверьте введенныe данныe")
			return
		}
		
		let request = AuthRequest(email: email, password: password)
		sendRequest(request)
	}
	
	func sendRequest(_ request: AuthRequest) {
		NetworkManager.shared.authorize(request: request) { response in
			Notifications.userAuthorized.post(with: response)
		}
	}
}
