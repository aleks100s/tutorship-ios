//
//  ActivityIndicatorSheet.swift
//  Polys
//
//  Created by Максим Бачурин on 23.11.2020.
//

import UIKit

class ActivityIndicatorSheet: UIViewController {

	static let activityWindow = UIWindow()

	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}

	class func addSheet() {
		let story = UIStoryboard(name: "SheetController", bundle: nil)
		let vc = story.instantiateViewController(withIdentifier: "ActivityIndicatorSheet") as! ActivityIndicatorSheet

		activityWindow.rootViewController = vc
		vc.view.backgroundColor = UIColor.clear
		activityWindow.backgroundColor = UIColor.clear
		activityWindow.isHidden = false
		activityWindow.windowLevel = UIWindow.Level(rawValue: 1)
		activityWindow.makeKeyAndVisible()

		vc.animateAppear()
	}

	class func hide() {
		if let vc = activityWindow.rootViewController as? ActivityIndicatorSheet{
			vc.animateDisAppear()
		}
	}

	func animateAppear() {
		UIView.animate(withDuration: 0.01) {
			self.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
		}
	}

	func animateDisAppear() {
		UIView.animate(withDuration: 0.3, animations: {
			self.view.backgroundColor = UIColor.clear
			self.view.layoutIfNeeded()
		}) { (finish: Bool) in
			self.view.window?.removeFromSuperview()
			self.view.window?.resignKey()
			self.view.window?.isHidden = true
		}
	}

}

