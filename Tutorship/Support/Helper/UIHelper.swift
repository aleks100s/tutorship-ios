//
//  UIHelper.swift
//  Tutorship
//
//  Created by Alexander on 22.05.2021.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class UIHelper {
	static func prepareTextFields(textFields: [MDCOutlinedTextField]) {
		for textField in textFields {
			textField.setTextColor(Colors.lightWhite, for: .normal)
			textField.setTextColor(Colors.white, for: .editing)
			textField.setTextColor(UIColor.lightText, for: .disabled)
			textField.setOutlineColor(Colors.accentWeak, for: .normal)
			textField.setOutlineColor(Colors.accent, for: .editing)
			textField.setOutlineColor(Colors.accentWeak, for: .disabled)
			textField.setNormalLabelColor(Colors.accentWeak, for: .normal)
			textField.setNormalLabelColor(Colors.accent, for: .editing)
			textField.setNormalLabelColor(Colors.accentWeak, for: .disabled)
			textField.setFloatingLabelColor(Colors.accentWeak, for: .normal)
			textField.setFloatingLabelColor(Colors.accent, for: .editing)
			textField.setFloatingLabelColor(Colors.accentWeak, for: .disabled)
			textField.setLeadingAssistiveLabelColor(Colors.red, for: .editing)
			textField.setLeadingAssistiveLabelColor(Colors.red, for: .normal)
		}
	}
	
	static func showErrorInField(_ textField: MDCOutlinedTextField, text: String) {
		textField.resignFirstResponder()
		textField.leadingAssistiveLabel.text = text
		textField.setOutlineColor(Colors.red, for: .normal)
		textField.setOutlineColor(Colors.red, for: .editing)
		textField.setFloatingLabelColor(Colors.red, for: .normal)
		textField.setFloatingLabelColor(Colors.red, for: .editing)
	}
	
	static func removeError(from textField: MDCOutlinedTextField) {
		textField.leadingAssistiveLabel.text = nil
		textField.setOutlineColor(Colors.accentWeak, for: .normal)
		textField.setOutlineColor(Colors.accent, for: .editing)
		textField.setFloatingLabelColor(Colors.accentWeak, for: .normal)
		textField.setFloatingLabelColor(Colors.accent, for: .editing)
	}
	
	static func setViewShadow(_ views: [UIView]) {
		for currentView in views {
			currentView.layer.shadowColor = UIColor.black.cgColor
			currentView.layer.shadowOpacity = 0.3
			currentView.layer.shadowRadius = 3
			currentView.layer.shadowOffset = CGSize(width: 0, height: 3)
			currentView.layer.shouldRasterize = true
			currentView.layer.rasterizationScale = UIScreen.main.scale
		}
	}
}
