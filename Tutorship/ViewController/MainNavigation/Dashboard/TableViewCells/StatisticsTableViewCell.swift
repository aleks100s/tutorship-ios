//
//  StatisticsTableViewCell.swift
//  Tutorship
//
//  Created by Alexander on 03.06.2021.
//

import UIKit

class StatisticsTableViewCell: UITableViewCell {

	@IBOutlet weak var chartView: UIView!
	@IBOutlet weak var statisticsLabel: UILabel!
	@IBOutlet weak var valueLabel: UILabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
		chartView.layer.cornerRadius = 4
    }
    
}
