//
//  StudentListViewController.swift
//  Tutorship
//
//  Created by Alexander on 23.05.2021.
//

import UIKit

class StudentListViewController: BaseViewController {
	override class var storyboardName: String { "Students" }

	@IBOutlet weak var tableView: UITableView!
	
	var viewModels: [StudentViewModel] = [] {
		didSet {
			tableView.reloadData()
		}
	}
	
	override func viewDidLoad() {
        super.viewDidLoad()
		navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addStudentPressed))
		setupTableView()
    }
	
	private func setupTableView() {		
		tableView.register(UINib(nibName: "StudentTableViewCell", bundle: nil), forCellReuseIdentifier: "StudentTableViewCell")
		tableView.delegate = self
		tableView.dataSource = self
	}
	
	override func viewWillAppear(_ animated: Bool) {
		loadData()
	}
	
	private func loadData() {
		StudentViewModel.loadData { studentViewModels in
			self.viewModels = studentViewModels
		}
	}
	
	@objc func addStudentPressed() {
		let viewController = StudentDetailViewController.controller()
		viewController.contentMode = .creating
		navigationController?.pushViewController(viewController, animated: true)
	}
}

extension StudentListViewController: UITableViewDataSource, UITableViewDelegate {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		viewModels.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "StudentTableViewCell", for: indexPath) as! StudentTableViewCell
		cell.viewModel = viewModels[indexPath.row]
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		let viewModel = viewModels[indexPath.row]
		let viewController = StudentDetailViewController.controller()
		viewController.contentMode = .simple
		viewController.viewModel = viewModel
		navigationController?.pushViewController(viewController, animated: true)
	}
	
	func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
			-> UISwipeActionsConfiguration? {
		let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in
			tableView.beginUpdates()
			let viewModel = self.viewModels.remove(at: indexPath.row)
			tableView.deleteRows(at: [indexPath], with: .fade)
			viewModel.delete()
			tableView.endUpdates()
			completionHandler(true)
		}
		deleteAction.image = UIImage(systemName: "trash")
		deleteAction.backgroundColor = .systemRed
		let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
		return configuration
	}
}
