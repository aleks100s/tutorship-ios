//
//  ComplexTransaction.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Foundation

class ComplexTransaction: Codable, BaseModel {
	var id: Int = 0
	var studentName: String = ""
	var amount: Double = 0
	var lessonID: Int = 0
	var timestamp: Date = Date()
	var lessonDate: Date = Date()
	var attendanceID: Int = 0
	
	required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		id = try container.decode(Int.self, forKey: .id)
		studentName = try container.decode(String.self, forKey: .studentName)
		amount = try container.decode(Double.self, forKey: .amount)
		lessonID = try container.decode(Int.self, forKey: .lessonID)
		let timestamp = try container.decode(Double.self, forKey: .timestamp)
		self.timestamp = Date(timeIntervalSince1970: timestamp / 1000)
		let date = try container.decode(Double.self, forKey: .lessonDate)
		self.lessonDate = Date(timeIntervalSince1970: date / 1000)
		attendanceID = try container.decode(Int.self, forKey: .attendanceID)
	}
	
	enum CodingKeys: String, CodingKey {
		case id
		case studentName = "fullName"
		case amount
		case lessonID
		case timestamp
		case lessonDate
		case attendanceID = "attendance"
	}
}
