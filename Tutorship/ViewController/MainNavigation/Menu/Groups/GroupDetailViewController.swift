//
//  GroupDetailViewController.swift
//  Tutorship
//
//  Created by Alexander on 23.05.2021.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class GroupDetailViewController: BaseViewController {
	override class var storyboardName: String { "Groups" }

	@IBOutlet weak var initialsView: InitialsView!
	@IBOutlet weak var groupNameTextField: MDCOutlinedTextField!
	@IBOutlet weak var subjectDropDown: MDCOutlinedTextField!
	@IBOutlet weak var selectSubjectButton: UIButton!
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var tableViewHeight: NSLayoutConstraint!
	
	var viewModel = GroupViewModel()
	var groupStudents: [StudentViewModel] = []
	var addedStudents: [StudentViewModel] = []
	var otherStudents: [StudentViewModel] = []
	var selectedSubject: Subject? {
		didSet {
			subjectDropDown.text = selectedSubject?.name
		}
	}
	override var contentMode: BaseViewController.ContentMode {
		didSet {
			if isViewLoaded {
				onContentModeChanged()
			}
		}
	}
	
	override func viewDidLoad() {
        super.viewDidLoad()
		setupTableView()
		setupViews()
		bindViewModel()
		onContentModeChanged()
		loadStudents()
    }
	
	private func setupTableView() {
		tableView.register(UINib(nibName: "StudentTableViewCell", bundle: nil), forCellReuseIdentifier: "StudentTableViewCell")
		tableView.dataSource = self
		tableView.delegate = self
		tableView.estimatedRowHeight = 64.5
		tableView.estimatedSectionHeaderHeight = 28
		tableView.estimatedSectionFooterHeight = 0
	}
	
	private func setupViews() {		
		UIHelper.prepareTextFields(textFields: [groupNameTextField, subjectDropDown])
		groupNameTextField.label.text = "Название группы"
		subjectDropDown.label.text = "Предмет"
		selectSubjectButton.imageView?.tintColor = Colors.accent
	}
	
	private func bindViewModel() {
		viewModel.initials.bind(listener: { value in
			self.initialsView.initialsLabel.text = value
		})
		viewModel.color.bind(listener: { color in
			self.initialsView.roundView.backgroundColor = color
		})
		viewModel.groupName.bind(listener: { value in
			self.groupNameTextField.text = value
		})
		viewModel.subject.bind(listener: { value in
			self.subjectDropDown.text = value
		})
	}
	
	private func onContentModeChanged() {
		var buttonTitle = "Создать"
		var selector = #selector(saveGroup)
		switch contentMode {
		case .creating:
			title = "Новая группа"
			buttonTitle = "Создать"
			selector = #selector(saveGroup)
			tableView.allowsSelection = true
			selectSubjectButton.isEnabled = true
			break
		case .simple:
			title = "Просмотр"
			buttonTitle = "Изменить"
			selector = #selector(editGroup)
			tableView.allowsSelection = false
			selectSubjectButton.isEnabled = false
			break
		case .editing:
			title = "Редактирование"
			buttonTitle = "Сохранить"
			selector = #selector(updateGroup)
			tableView.allowsSelection = true
			selectSubjectButton.isEnabled = true
			break
		}
		navigationItem.rightBarButtonItem = UIBarButtonItem(title: buttonTitle, style: .plain, target: self, action: selector)
		let enabled = contentMode != .simple
		groupNameTextField.isEnabled = enabled
		subjectDropDown.isEnabled = enabled
		
		tableView.reloadData()
		tableViewHeight.constant = tableView.contentSize.height
	}
	
	@IBAction func selectSubjectPressed(_ sender: Any) {
		let controller = PickerViewController.controller()
		controller.pickerMode = .subject
		controller.delegate = self
		let navController = UINavigationController(rootViewController: controller)
		present(navController, animated: true)
	}
	
	@objc func saveGroup() {
		let name = groupNameTextField.text ?? ""
		let subjectID = selectedSubject?.id ?? 0
		
		ActivityIndicatorSheet.addSheet()
		viewModel.create(name: name, subjectID: subjectID, students: addedStudents) { [weak self] in
			ActivityIndicatorSheet.hide()
			self?.contentMode = .simple
		}
	}

	@objc func editGroup() {
		contentMode = .editing
	}
	
	@objc func updateGroup() {
		let name = groupNameTextField.text ?? ""
		let subjectID = selectedSubject?.id ?? 0
		
		let insertedStudents = addedStudents.filter { student in
			!groupStudents.contains(where: {$0.id.value == student.id.value})
		}
		
		let removedStudents = groupStudents.filter { student in
			!addedStudents.contains(where: {$0.id.value == student.id.value})
		}
		
		ActivityIndicatorSheet.addSheet()
		viewModel.update(name: name, subjectID: subjectID, insertedStudents: insertedStudents, removedStudents: removedStudents) { [weak self] in
			ActivityIndicatorSheet.hide()
			self?.contentMode = .simple
		}
	}
	
	private func loadStudents() {
		StudentViewModel.loadData { [weak self] allStudents in
			self?.viewModel.getGroupStudents(completion: { groupStudents in
				self?.groupStudents = Array(groupStudents)
				self?.addedStudents = groupStudents
				self?.otherStudents = allStudents.filter({ student in
					!groupStudents.contains(where: {$0.id.value == student.id.value})
				})
				self?.tableView.reloadData()
				self?.tableViewHeight.constant = self?.tableView.contentSize.height ?? 0
			})
		}
	}
}

extension GroupDetailViewController: UITableViewDataSource, UITableViewDelegate {
	func numberOfSections(in tableView: UITableView) -> Int {
		contentMode == .simple ? 1 : 2
	}
	
	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		if contentMode == .simple {
			return nil
		} else {
			return 	section == 0 ? "Добавленные ученики" : "Другие ученики"
		}
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if section == 0 {
			return addedStudents.count
		} else {
			return otherStudents.count
		}
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "StudentTableViewCell", for: indexPath) as! StudentTableViewCell
		if indexPath.section == 0 {
			cell.viewModel = addedStudents[indexPath.row]
		} else {
			cell.viewModel = otherStudents[indexPath.row]
		}
		return cell
	}
		
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		if contentMode != .simple {
			tableView.beginUpdates()
			if indexPath.section == 0 { // added
				let student = addedStudents.remove(at: indexPath.row)
				tableView.deleteRows(at: [indexPath], with: .right)
				otherStudents.insert(student, at: 0)
				tableView.insertRows(at: [IndexPath(row: 0, section: 1)], with: .left)
			} else { // other
				let student = otherStudents.remove(at: indexPath.row)
				tableView.deleteRows(at: [indexPath], with: .left)
				addedStudents.insert(student, at: 0)
				tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .left)
			}
			tableView.endUpdates()
		}
	}
}

extension GroupDetailViewController: PickerViewControllerDelegate {
	func picker(_ controller: PickerViewController, didSelect value: Any) {
		if let subject = value as? Subject {
			self.selectedSubject = subject
		}
	}
}
