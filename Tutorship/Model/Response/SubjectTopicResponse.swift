//
//  SubjectTopicResponse.swift
//  Tutorship
//
//  Created by Alexander on 18.05.2021.
//

import Foundation

class SubjectTopicResponse: Decodable, BaseModel {
	var topics: [String]
}
