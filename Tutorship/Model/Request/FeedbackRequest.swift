//
//  FeedbackRequest.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Foundation

class FeedbackRequest: Encodable, BaseModel {
	internal init(email: String, text: String) {
		self.email = email
		self.text = text
	}
	
	var email: String
	var text: String
}
