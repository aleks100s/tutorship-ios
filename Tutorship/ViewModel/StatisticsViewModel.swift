//
//  StatisticsViewModel.swift
//  Tutorship
//
//  Created by Alexander on 10.06.2021.
//

import Foundation

class StatisticsViewModel {
	
	var attendancePercent: LiveData<String> = LiveData("")
	var totalIncome: LiveData<String> = LiveData("")
	var lessonsCount: LiveData<String> = LiveData("")
	
	init(groupID: Int = 0, start: Date, end: Date) {
		loadAttendance(groupID: groupID, start: start.timeIntervalSince1970 * 1000, end: end.timeIntervalSince1970 * 1000)
		loadTotalIncome(groupID: groupID, start: start.timeIntervalSince1970 * 1000, end: end.timeIntervalSince1970 * 1000)
		loadLessonsCount(groupID: groupID, start: start.timeIntervalSince1970 * 1000, end: end.timeIntervalSince1970 * 1000)
	}
	
	func loadAttendance(groupID: Int, start: Double, end: Double) {
		Repository.shared.getAttendancePercent(start: start, end: end, groupID: groupID) { value in
			self.attendancePercent.value = "\(value)"
		}
	}
	
	func loadTotalIncome(groupID: Int, start: Double, end: Double) {
		Repository.shared.getTotalIncome(start: start, end: end, groupID: groupID) { value in
			self.totalIncome.value = "\(value)"
		}
	}
	
	func loadLessonsCount(groupID: Int, start: Double, end: Double) {
		Repository.shared.getLessonsCount(start: start, end: end, groupID: groupID) { value in
			self.lessonsCount.value = "\(value)"
		}
	}
	
}
