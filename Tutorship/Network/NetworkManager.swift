//
//  NetworkManager.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Moya
import Alamofire

final class NetworkManager {
	let provider = MoyaProvider<NetworkService>()
	
	static let shared = NetworkManager()
	
	private init() {}
	
	private func processRequestWithoutResponse(_ result: Result<Response, MoyaError>, requestResult: @escaping (_ success: Bool) -> Void) {
		switch result {
		case .success(let response):
			if 200...399 ~= response.statusCode {
				requestResult(true)
			} else if response.statusCode == 422 {
				postError(.unauthorized)
			} else {
				postError(.errorWithText(String(data: response.data, encoding: .utf8) ?? "Ошибка. Код ответа \(response.statusCode)"))
			}
		case .failure(let error):
			postError(.errorWithText(error.localizedDescription))
		}
	}
	
	private func processSimpleResult<T>(_ result: Result<Response, MoyaError>, requestResult: @escaping (_ object: T) -> Void) where T: Decodable {
		switch result {
		case .success(let response):
			if 200...399 ~= response.statusCode {
				do {
					let object = try JSONDecoder().decode(T.self, from: response.data)
					print(object)
					requestResult(object)
				} catch {
					print(String(data: response.data, encoding: .utf8))
					postError(.errorWithText(error.localizedDescription))
				}
			} else if response.statusCode == 422 {
				postError(.unauthorized)
			} else {
				let text = String(data: response.data, encoding: .utf8) ?? "Ошибка. Код ответа \(response.statusCode)"
				postError(.errorWithText(text))
			}
		case .failure(let error):
			postError(.errorWithText(error.localizedDescription))
		}
	}
	
	func postError(_ error: MyError?) {
		ActivityIndicatorSheet.hide()
		Notifications.errorOccured.post(with: error?.description)
	}
	
	func register(request: AuthRequest, requestResult: @escaping (_ result: Bool) -> Void) {
		provider.request(.register(request: request)) { [weak self] result in
			self?.processRequestWithoutResponse(result, requestResult: requestResult)
		}
	}
	
	func authorize(request: AuthRequest, requestResult: @escaping (_ authObject: AuthResponse) -> Void) {
		provider.request(.authorize(request: request)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getProfileInfo(requestResult: @escaping (_ user: UserResponse) -> Void) {
		provider.request(.getProfileInfo) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func updateProfile(profile: ProfileRequest, requestResult: @escaping (_ profile: UserResponse) -> Void) {
		provider.request(.updateProfile(request: profile)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getAllSubjects(requestResult: @escaping (_ subjects: [Subject]) -> Void) {
		provider.request(.getAllSubjects) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func createSubject(subject: Subject, requestResult: @escaping (_ subject: Subject) -> Void) {
		provider.request(.createSubject(subject: subject)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getSubjectTopics(subjectID: Int, requestResult: @escaping (_ response: SubjectTopicResponse) -> Void) {
		provider.request(.getSubjectTopics(subjectID: subjectID)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getAllStudents(includeArchived: Bool, requestResult: @escaping (_ students: [Student]) -> Void) {
		provider.request(.getAllStudents(includeArchived: includeArchived)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func createStudent(student: Student, requestResult: @escaping (_ student: Student) -> Void) {
		provider.request(.createStudent(student: student)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getStudent(studentID: Int, requestResult: @escaping (_ response: Student) -> Void) {
		provider.request(.getStudent(id: studentID)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func updateStudent(student: Student, requestResult: @escaping (_ response: Student) -> Void) {
		provider.request(.updateStudent(id: student.id, student: student)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func deleteStudent(studentID: Int, requestResult: @escaping (_ success: Bool) -> Void) {
		provider.request(.deleteStudent(id: studentID)) { [weak self] result in
			self?.processRequestWithoutResponse(result, requestResult: requestResult)
		}
	}
	
	func getAllGroups(includeIndividual: Bool, requestResult: @escaping (_ groups: [Group]) -> Void) {
		provider.request(.getAllGroups(includeIndividual: includeIndividual)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func createGroup(group: Group, requestResult: @escaping (_ group: Group) -> Void) {
		provider.request(.createGroup(group: group)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getGroup(groupID: Int, requestResult: @escaping (_ group: Group) -> Void) {
		provider.request(.getGroup(id: groupID)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func updateGroup(group: Group, requestResult: @escaping (_ group: Group) -> Void) {
		provider.request(.updateGroup(id: group.id, group: group)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func deleteGroup(groupID: Int, requestResult: @escaping (_ success: Bool) -> Void) {
		provider.request(.deleteGroup(id: groupID)) { [weak self] result in
			self?.processRequestWithoutResponse(result, requestResult: requestResult)
		}
	}
	
	func createLinkAndProgress(link: Link, requestResult: @escaping (_ link: Link) -> Void) {
		provider.request(.createLinkAndProgress(link: link)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getLinksForGroup(groupID: Int, requestResult: @escaping (_ links: [Link]) -> Void) {
		provider.request(.getLinksForGroup(id: groupID)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func deleteLink(groupID: Int, studentID: Int, requestResult: @escaping (_ success: Bool) -> Void) {
		provider.request(.deleteLink(groupID: groupID, studentID: studentID)) { [weak self] result in
			self?.processRequestWithoutResponse(result, requestResult: requestResult)
		}
	}
	
	func createLesson(lesson: Lesson, requestResult: @escaping (_ lesson: Lesson) -> Void) {
		provider.request(.createLesson(lesson: lesson)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getLessons(dateFrom: Double, dateTo: Double, after: Double, groupID: Int, requestResult: @escaping (_ lessons: [FullLesson]) -> Void) {
		provider.request(.getLessons(dateFrom: dateFrom, dateTo: dateTo, after: after, groupID: groupID)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getFullLesson(lessonID: Int, requestResult: @escaping (_ lesson: Lesson) -> Void) {
		provider.request(.getFullLesson(id: lessonID)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func deleteLesson(lessonID: Int, requestResult: @escaping (_ success: Bool) -> Void) {
		provider.request(.deleteLesson(id: lessonID)) { [weak self] result in
			self?.processRequestWithoutResponse(result, requestResult: requestResult)
		}
	}
	
	func updateLesson(lesson: Lesson, requestResult: @escaping (_ lesson: Lesson) -> Void) {
		provider.request(.updateLesson(id: lesson.id, lesson: lesson)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func finishLesson(lessonID: Int, request: FinishLessonRequest, requestResult: @escaping (_ success: Bool) -> Void) {
		provider.request(.finishLesson(id: lessonID, request: request)) { [weak self] result in
			self?.processRequestWithoutResponse(result, requestResult: requestResult)
		}
	}
	
	func getStudentsForGroup(groupID: Int, requestResult: @escaping (_ students: [Student]) -> Void) {
		provider.request(.getStudentsOfGroup(id: groupID)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getStudentDiscount(groupID: Int, studentID: Int, requestResult: @escaping (_ discount: DiscountResponse) -> Void) {
		provider.request(.getStudentDiscount(groupID: groupID, studentID: studentID)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getAttendances(groupID: Int, lessonID: Int, requestResult: @escaping (_ attendances: [FullAttendance]) -> Void) {
		provider.request(.getAttendances(lessonID: lessonID, groupID: groupID)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func updateAttendance(attendance: Attendance, requestResult: @escaping (_ attendance: Attendance) -> Void) {
		provider.request(.updateAttendance(id: attendance.id, attendance: attendance)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func payForLesson(transaction: Transaction, requestResult: @escaping (_ transaction: Transaction) -> Void) {
		provider.request(.payForLesson(transaction: transaction)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func createHomework(homework: Homework, requestResult: @escaping (_ homework: Homework) -> Void) {
		provider.request(.createHomework(homework: homework)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getHomework(groupID: Int, lessonID: Int, requestResult: @escaping (_ homework: [FullHomework]) -> Void) {
		provider.request(.getHomework(groupID: groupID, lessonID: lessonID)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func lessonHasHomework(lessonID: Int, requestResult: @escaping (_ response: HasHomeworkResponse) -> Void) {
		provider.request(.lessonHasHomework(lessonID: lessonID)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getAttendance(attendanceID: Int, requestResult: @escaping (_ attendance: FullAttendance) -> Void) {
		provider.request(.getAttendance(id: attendanceID)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getProgresses(groupID: Int, requestResult: @escaping (_ progresses: [FullProgress]) -> Void) {
		provider.request(.getProgresses(groupID: groupID)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getProgress(progressID: Int, requestResult: @escaping (_ progress: FullProgress) -> Void) {
		provider.request(.getProgress(id: progressID)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func updateProgress(progress: Progress, requestResult: @escaping (_ progress: Progress) -> Void) {
		provider.request(.updateProgress(id: progress.id, progress: progress)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getAttendanceFraction(progressID: Int, requestResult: @escaping (_ response: AttendanceFractionResponse) -> Void) {
		provider.request(.getAttendanceFraction(progressID: progressID)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getDebt(progressID: Int, requestResult: @escaping (_ response: DebtResponse) -> Void) {
		provider.request(.getDebt(progressID: progressID)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func refillBalance(balanceRefill: BalanceRefill, requestResult: @escaping (_ refill: BalanceRefill) -> Void) {
		provider.request(.refillBalance(balanceRefill: balanceRefill)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getTasks(groupID: Int, requestResult: @escaping (_ tasks: [ComplexTask]) -> Void) {
		provider.request(.getTasks(groupID: groupID)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getTask(taskID: Int, requestResult: @escaping (_ task: ComplexTask) -> Void) {
		provider.request(.getTask(id: taskID)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func updateTask(task: Task, requestResult: @escaping (_ task: Task) -> Void) {
		provider.request(.updateTask(id: task.id, task: task)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getTransactions(last: Int, requestResult: @escaping (_ transactions: [ComplexTransaction]) -> Void) {
		provider.request(.getTransactions(last: last)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getGroupTopics(groupID: Int, requestResult: @escaping (_ response: GroupTopicsResponse) -> Void) {
		provider.request(.getGroupTopics(groupID: groupID)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getExpiredTasks(requestResult: @escaping (_ tasks: [ComplexTask]) -> Void) {
		provider.request(.getExpiredTasks) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getDebtors(requestResult: @escaping (_ attendances: [FullAttendance]) -> Void) {
		provider.request(.getDebtors) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getAttendancePercent(start: Double, end: Double, groupID: Int, requestResult: @escaping (_ response: AttendancePercentResponse) -> Void) {
		provider.request(.getAttendancePercent(start: start, end: end, groupID: groupID)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getTotalIncome(start: Double, end: Double, groupID: Int, requestResult: @escaping (_ response: TotalIncomeResponse) -> Void) {
		provider.request(.getTotalIncome(start: start, end: end, groupID: groupID)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getLessonsCount(start: Double, end: Double, groupID: Int, requestResult: @escaping (_ response: LessonsCountResponse) -> Void) {
		provider.request(.getLessonsCount(start: start, end: end, groupID: groupID)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getTotalIncomeWithLastYear(start: Double, end: Double, requestResult: @escaping (_ response: TotalIncomeWithLastYearResponse) -> Void) {
		provider.request(.getTotalIncomeWithLastYear(start: start, end: end)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func getLessonsCountWithLastYear(start: Double, end: Double, requestResult: @escaping (_ response: LessonsCountWithLastYearResponse) -> Void) {
		provider.request(.getLessonsCountWithLastYear(start: start, end: end)) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func postFeedback(feedback: FeedbackRequest, requestResult: @escaping (_ success: Bool) -> Void) {
		provider.request(.postFeedback(request: feedback)) { [weak self] result in
			self?.processRequestWithoutResponse(result, requestResult: requestResult)
		}
	}
	
	func getNotifications(requestResult: @escaping (_ notifications: [MyNotification]) -> Void) {
		provider.request(.getNotifications) { [weak self] result in
			self?.processSimpleResult(result, requestResult: requestResult)
		}
	}
	
	func deleteNotification(notificationID: Int, requestResult: @escaping (_ success: Bool) -> Void) {
		provider.request(.getNotifications) { [weak self] result in
			self?.processRequestWithoutResponse(result, requestResult: requestResult)
		}
	}
	
	func getTotalIncomeExport(start: Double, end: Double, groupID: Int, requestResult: @escaping (_ success: Bool) -> Void) {
		provider.request(.getTotalIncomeExport(start: start, end: end, groupID: groupID)) { [weak self] result in
			self?.processRequestWithoutResponse(result, requestResult: requestResult)
		}
	}
	
	func getLessonsCountExport(start: Double, end: Double, groupID: Int, requestResult: @escaping (_ success: Bool) -> Void) {
		provider.request(.getLessonsCountExport(start: start, end: end, groupID: groupID)) { [weak self] result in
			self?.processRequestWithoutResponse(result, requestResult: requestResult)
		}
	}
}
