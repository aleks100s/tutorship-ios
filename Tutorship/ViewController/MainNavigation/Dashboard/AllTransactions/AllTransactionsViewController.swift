//
//  AllTransactionsViewController.swift
//  Tutorship
//
//  Created by Alexander on 14.06.2021.
//

import UIKit
import MaterialComponents.MaterialBottomSheet

class AllTransactionsViewController: BaseViewController {
	override class func controller() -> Self {
		return AllTransactionsViewController(nibName: String(describing: Self.self), bundle: nil) as! Self
	}

	@IBOutlet weak var tableView: UITableView!
	
	var transactions: [TransactionViewModel] = [] {
		didSet {
			tableView.reloadData()
		}
	}
	
	override func viewDidLoad() {
        super.viewDidLoad()
		setupTableView()
		loadTransactions()
    }
	
	private func setupTableView() {
		tableView.register(UINib(nibName: "TransactionTableViewCell", bundle: nil), forCellReuseIdentifier: "TransactionTableViewCell")
		tableView.dataSource = self
		tableView.delegate = self
	}
	
	private func loadTransactions() {
		TransactionViewModel.loadAllTransactions { transactions in
			self.transactions = transactions
		}
	}
}

extension AllTransactionsViewController: UITableViewDataSource, UITableViewDelegate {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		transactions.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTableViewCell", for: indexPath) as! TransactionTableViewCell
		if indexPath.row > 0 {
			cell.setupCell(with: transactions[indexPath.row - 1])
		}
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		if indexPath.row > 0 {
			let controller = TransactionViewController.controller()
			controller.transaction = transactions[indexPath.row - 1]
			let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: controller)
			present(bottomSheet, animated: true, completion: nil)
		}
	}
}
