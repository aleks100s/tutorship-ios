//
//  Colors.swift
//  Tutorship
//
//  Created by Alexander on 22.05.2021.
//

import UIKit

class Colors {
	static let accent = UIColor(named: "Accent")!
	static let accentWeak = UIColor(named: "AccentWeak")!
	static let accentDisabled = UIColor(named: "AccentVeryWeak")!
	static let primary = UIColor(named: "Primary")!
	static let primaryDark = UIColor(named: "PrimaryDark")!
	static let secondary = UIColor(named: "Secondary")!
	static let white = UIColor.white
	static let lightWhite = UIColor(hex: "EEEEEE")!
	static let red = UIColor.systemRed
}
