//
//  Link.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Foundation

class Link: Codable, BaseModel {
	var id: Int = 0
	var studentID: Int = 0
	var groupID: Int = 0
		
	enum CodingKeys: String, CodingKey {
		case id
		case studentID = "student"
		case groupID = "group"
	}
}
