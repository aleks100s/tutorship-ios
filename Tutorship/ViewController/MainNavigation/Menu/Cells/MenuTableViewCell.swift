//
//  MenuTableViewCell.swift
//  Tutorship
//
//  Created by Alexander on 23.05.2021.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

	@IBOutlet weak var menuIcon: UIImageView!
	@IBOutlet weak var menuTitle: UILabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
