//
//  Notifications.swift
//  Tutorship
//
//  Created by Alexander on 24.05.2021.
//

import Foundation

enum Notifications: String {
	case userAuthorized = "UserAuthorized"
	case signOut = "SignOut"
	case previewWatched = "PreviewWatched"
	case errorOccured = "ErrorOccured"
	case attendanceChanged = "AttendanceChanged"
	case taskChanged = "TaskChanged"
	
	var name: NSNotification.Name {
		return NSNotification.Name(self.rawValue)
	}
	
	func post(with object: Any? = nil) {
		NotificationCenter.default.post(name: name, object: object)
	}
	
	func subscribe(_ object: Any?, selector: Selector) {
		NotificationCenter.default.addObserver(object, selector: selector, name: name, object: nil)
	}
}
