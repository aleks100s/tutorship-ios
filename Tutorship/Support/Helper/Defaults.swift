//
//  Defaults.swift
//  Tutorship
//
//  Created by Alexander on 22.05.2021.
//

import Foundation


enum DefaultsKey: String, CaseIterable {
	case previewWatched
	case token
	case userID
	case pushToken
}

class Defaults: NSObject {
	private static func defaults() -> UserDefaults {
		return UserDefaults.standard
	}

	static func deleteAllKey() {
		if let appDomain = Bundle.main.bundleIdentifier {
			UserDefaults.standard.removePersistentDomain(forName: appDomain)
		}
	}
}

//TODO

extension Defaults {
	
	static var previewWatched: Bool {
		get{
			defaults().bool(forKey: DefaultsKey.previewWatched.rawValue)
		}
		set {
			defaults().set(newValue, forKey: DefaultsKey.previewWatched.rawValue)
		}
	}

	static var token: String? {
		get{
			defaults().string(forKey: DefaultsKey.token.rawValue)
		}
		set {
			defaults().set(newValue, forKey: DefaultsKey.token.rawValue)
		}
	}


	static var pushToken: String? {
		get{
			defaults().string(forKey: DefaultsKey.pushToken.rawValue)
		}
		set {
			defaults().set(newValue, forKey: DefaultsKey.pushToken.rawValue)
		}
	}
	
	static var userID: Int? {
		get{
			defaults().integer(forKey: DefaultsKey.userID.rawValue)
		}
		set {
			defaults().set(newValue, forKey: DefaultsKey.userID.rawValue)
		}
	}
}

extension Defaults {
	private static func archive(object: Any) -> Data {
		return NSKeyedArchiver.archivedData(withRootObject: object)
	}

	private static func unrchive(data: Data) -> Any? {
		return NSKeyedUnarchiver.unarchiveObject(with: data)
	}
}
