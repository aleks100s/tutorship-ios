//
//  HomeworkViewController.swift
//  Tutorship
//
//  Created by Alexander on 22.07.2021.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class HomeworkViewController: BaseViewController {
	
	override class func controller() -> Self {
		HomeworkViewController(nibName: String(describing: Self.self), bundle: nil) as! Self
	}

	@IBOutlet weak var contentTextView: UITextView!
	@IBOutlet weak var commentTextField: MDCOutlinedTextField!
	@IBOutlet weak var deadlineTextField: MDCOutlinedTextField!
	@IBOutlet weak var saveButton: UIButton!
	
	var lessonID: Int?
	var groupID: Int?
	private var deadline: Date?
	private var homework: Homework?
	
	private var datePicker: UIDatePicker = {
		let datePicker = UIDatePicker()
		datePicker.calendar = Calendar.current
		datePicker.locale = Locale(identifier: "ru_RU")
		datePicker.datePickerMode = .date
		if #available(iOS 13.4, *) {
			datePicker.preferredDatePickerStyle = .wheels
		}
		datePicker.minimumDate = Date()
		return datePicker
	}()
	
	override func viewDidLoad() {
        super.viewDidLoad()
		setupViews()
		if contentMode == .simple {
			loadHomework()
		} else {
			saveButton.isHidden = false
		}
    }
	
	private func setupViews() {
		UIHelper.prepareTextFields(textFields: [commentTextField, deadlineTextField])
		commentTextField.label.text = "Комментарий к ДЗ"
		deadlineTextField.label.text = "Деадлайн сдачи ДЗ"
		deadlineTextField.inputView = datePicker
		datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
		contentTextView.layer.cornerRadius = 4
		contentTextView.layer.borderWidth = 1
		contentTextView.layer.borderColor = Colors.accentWeak.cgColor
		self.title = "Новое домашнее задание"
	}
	
	private func loadHomework() {
		Repository.shared.loadHomework(groupID: groupID ?? 0, lessonID: lessonID ?? 0) { [weak self] homework in
			self?.homework = homework.homework
			self?.presentHomework()
		}
	}
	
	private func presentHomework() {
		if let homework = self.homework {
			let dateFormatter = DateFormatter()
			dateFormatter.dateFormat = "dd MMMM yyyy"
			dateFormatter.locale = Locale(identifier: "ru_RU")
			self.title = "ДЗ от \(dateFormatter.string(from: homework.deadline))"
			self.deadlineTextField.text = dateFormatter.string(from: homework.deadline)
			self.commentTextField.text = homework.comment
			self.contentTextView.text = homework.content
			saveButton.isHidden = true
		}
	}
	
	@objc func dateChanged(_ sender: UIDatePicker) {
		let date = sender.date
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "dd MMMM yyyy"
		dateFormatter.locale = Locale(identifier: "ru_RU")
		deadlineTextField.text = dateFormatter.string(from: date)
		self.deadline = date
	}
	
	@IBAction func saveHomeworkPressed(_ sender: Any) {
		let content = contentTextView.text ?? ""
		let date = deadline ?? Date()
		let comment = commentTextField.text ?? ""
		
		let homework = Homework(lessonID: lessonID ?? 0, content: content, comment: comment, deadline: date)
		Repository.shared.createHomework(homework: homework) { [weak self] homework in
			self?.homework = homework
			self?.presentHomework()
			self?.showAlert(title: "Домашнее задание успешно добавлено", message: nil, action: {
				self?.navigationController?.popViewController(animated: true)
			})
		}
	}
}
