//
//  MyNotification.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Foundation

class MyNotification: Codable, BaseModel {
	var id: Int = 0
	var title: String = ""
	var body: String = ""
	var action: String = ""
	var lessonID: Int = 0
	var attendanceID: Int = 0
	var datetime: Date = Date()
	
	required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		id  = try container.decode(Int.self, forKey: .id)
		title = try container.decode(String.self, forKey: .title)
		body = try container.decode(String.self, forKey: .body)
		action = try container.decode(String.self, forKey: .action)
		lessonID = try container.decode(Int.self, forKey: .lessonID)
		attendanceID = try container.decode(Int.self, forKey: .attendanceID)
		let datetime = try container.decode(Double.self, forKey: .datetime)
		self.datetime = Date(timeIntervalSince1970: datetime / 1000)
	}
	
	enum CodingKeys: String, CodingKey {
		case id
		case title
		case body
		case action
		case lessonID = "lesson"
		case attendanceID = "attendance"
		case datetime
	}
}
