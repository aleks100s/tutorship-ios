//
//  StudentTableViewCell.swift
//  Tutorship
//
//  Created by Alexander on 25.05.2021.
//

import UIKit

class StudentTableViewCell: UITableViewCell {

	@IBOutlet weak var initialsRoundView: InitialsView!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var phoneLabel: UILabel!
	
	var viewModel: StudentViewModel? {
		didSet {
			self.bindViewModel()
		}
	}
	
	override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
		self.backgroundColor = selected ? Colors.accentWeak : .clear
    }
	
	func bindViewModel() {
		viewModel?.initials.bind { value in
			self.initialsRoundView.initialsLabel.text = value
		}
		viewModel?.color.bind(listener: { color in
			self.initialsRoundView.roundView.backgroundColor = color
		})
		viewModel?.fullName.bind { value in
			self.nameLabel.text = value
		}
		viewModel?.phoneNumber.bind { value in
			self.phoneLabel.text = value
		}
	}
    
}
