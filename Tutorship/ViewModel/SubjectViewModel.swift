//
//  SubjectViewModel.swift
//  Tutorship
//
//  Created by Alexander on 27.05.2021.
//

import Foundation

class SubjectViewModel {
	static func loadData(completion: @escaping ([SubjectViewModel]) -> Void) {
		Repository.shared.getSubjectsData { subjects in
			let viewModels = subjects.map { subject in
				SubjectViewModel(subject: subject)
			}
			completion(viewModels)
		}
	}
	
	let id: LiveData<Int> = LiveData(0)
	let name: LiveData<String> = LiveData("")
	
	init(subject: Subject) {
		self.id.value = subject.id
		self.name.value = subject.name
	}
}
