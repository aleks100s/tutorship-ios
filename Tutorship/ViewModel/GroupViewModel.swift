//
//  GroupViewModel.swift
//  Tutorship
//
//  Created by Alexander on 26.05.2021.
//

import Foundation
import UIKit.UIColor

class GroupViewModel {
	static func loadData(completion: @escaping ([GroupViewModel]) -> Void) {
		Repository.shared.getGroupsData { groups in
			let viewModels = groups.map { group in
				GroupViewModel(group: group)
			}
			completion(viewModels)
		}
	}
	
	init() {}
	
	init(group: Group) {
		setupFields(group: group)
	}
	
	private func setupFields(group: Group) {
		let name = group.name
		let parts = name.split(separator: " ")
		if parts.count > 1 {
			initials.value = "\(parts[0].localizedCapitalized.first ?? " ")\(parts[1].localizedCapitalized.first ?? " ")"
		} else {
			if name.count > 1 {
				let index = name.index(name.startIndex, offsetBy: 1)
				initials.value = String(name[..<index])
			} else {
				initials.value = "\(name.first ?? " ")"
			}
		}
		color.value = UIColor(hex: group.color) ?? .white
		groupName.value = group.name
		subject.value = group.subject
		subjectID.value = group.subjectID
		id.value = group.id
	}
	
	let initials: LiveData<String> = LiveData("")
	let color: LiveData<UIColor> = LiveData(Colors.white)
	let groupName: LiveData<String> = LiveData("")
	let subject: LiveData<String> = LiveData("")
	let subjectID: LiveData<Int> = LiveData(0)
	let id: LiveData<Int> = LiveData(0)
	
	func create(name: String, subjectID: Int, students: [StudentViewModel], completion: @escaping () -> Void) {
		let group = Group(name: name, subjectID: subjectID)
		Repository.shared.createGroup(group: group) { [weak self] group in
			self?.setupFields(group: group)
			self?.insertLinks(for: students)
			completion()
		}
	}
	
	func update(name: String, subjectID: Int, insertedStudents: [StudentViewModel], removedStudents: [StudentViewModel], completion: @escaping () -> Void) {
		let group = Group(id: id.value, name: name, subjectID: subjectID)
		Repository.shared.updateGroup(group: group) { [weak self] group in
			self?.setupFields(group: group)
			self?.insertLinks(for: insertedStudents)
			self?.deleteLinks(for: removedStudents)
			completion()
		}
	}
	
	private func insertLinks(for students: [StudentViewModel]) {
		for student in students {
			let link = Link()
			link.groupID = id.value
			link.studentID = student.id.value
			Repository.shared.createLink(link: link)
		}
	}
	
	private func deleteLinks(for students: [StudentViewModel]) {
		for student in students {
			let link = Link()
			link.groupID = id.value
			link.studentID = student.id.value
			Repository.shared.deleteLink(link: link)
		}
	}
	
	func delete() {
		Repository.shared.deleteGroup(groupID: id.value)
	}
	
	func getGroupStudents(completion: @escaping ([StudentViewModel]) -> Void) {
		if id.value == 0 {
			completion([])
		} else {
			Repository.shared.getGroupStudentsData(groupID: id.value) { students in
				let viewModels = students.map({StudentViewModel(student: $0)})
				completion(viewModels)
			}
		}
	}
	
	static func getGroupStudents(groupID: Int, completion: @escaping ([StudentViewModel]) -> Void) {
		if groupID == 0{
			completion([])
		} else {
			Repository.shared.getGroupStudentsData(groupID: groupID) { students in
				let viewModels = students.map({StudentViewModel(student: $0)})
				completion(viewModels)
			}
		}
	}
}
