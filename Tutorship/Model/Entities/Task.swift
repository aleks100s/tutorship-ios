//
//  Task.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Foundation

class Task: Codable, BaseModel {
	var id: Int = 0
	var homeworkID: Int = 0
	var studentID: Int = 0
	var mark: Int = 0
	var deadline: Date = Date()
	var passedAt: Date = Date()
	var teacherComment: String = ""
	
	init(id: Int, homeworkID: Int, studentID: Int, mark: Int, deadline: Date, passedAt: Date, comment: String) {
		self.id = id
		self.homeworkID = homeworkID
		self.studentID = studentID
		self.mark = mark
		self.deadline = deadline
		self.passedAt = passedAt
		self.teacherComment = comment
	}
	
	required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		id = try container.decode(Int.self, forKey: .id)
		homeworkID = try container.decode(Int.self, forKey: .homeworkID)
		studentID = try container.decode(Int.self, forKey: .studentID)
		mark = try container.decode(Int.self, forKey: .mark)
		let deadline = try container.decode(Double.self, forKey: .deadline)
		self.deadline = Date(timeIntervalSince1970: deadline / 1000)
		let passedAt = try container.decode(Double.self, forKey: .passedAt)
		self.passedAt = Date(timeIntervalSince1970: passedAt / 1000)
		teacherComment = try container.decode(String.self, forKey: .teacherComment)
	}
	
	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(id, forKey: .id)
		try container.encode(homeworkID, forKey: .homeworkID)
		try container.encode(studentID, forKey: .studentID)
		try container.encode(mark, forKey: .mark)
		try container.encode(deadline.timeIntervalSince1970.rounded() * 1000, forKey: .deadline)
		try container.encode(passedAt.timeIntervalSince1970.rounded() * 1000, forKey: .passedAt)
		try container.encode(teacherComment, forKey: .teacherComment)
	}
	
	enum CodingKeys: String, CodingKey {
		case id
		case homeworkID = "homework"
		case studentID = "student"
		case mark
		case deadline
		case passedAt = "passed"
		case teacherComment = "comment"
	}
}
