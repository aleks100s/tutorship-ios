//
//  Transaction.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Foundation

class Transaction: Codable, BaseModel {
	var id: Int = 0
	var attendanceID: Int = 0
	var timestamp: Date = Date()
	var amount: Double = 0
	
	init() {}
	
	required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		id = try container.decode(Int.self, forKey: .id)
		attendanceID = try container.decode(Int.self, forKey: .attendanceID)
		let timestamp = try container.decode(Double.self, forKey: .timestamp)
		self.timestamp = Date(timeIntervalSince1970: timestamp / 1000)
		amount = try container.decode(Double.self, forKey: .amount)
	}
	
	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(id, forKey: .id)
		try container.encode(attendanceID, forKey: .attendanceID)
		try container.encode(timestamp.timeIntervalSince1970.rounded() * 1000, forKey: .timestamp)
		try container.encode(amount, forKey: .amount)
	}
	
	enum CodingKeys: String, CodingKey {
		case id
		case attendanceID = "attendance"
		case timestamp
		case amount
	}
}
