//
//  FeedbackViewController.swift
//  Tutorship
//
//  Created by Alexander on 23.05.2021.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class FeedbackViewController: BaseViewController {
	override class var storyboardName: String { "Feedback" }

	@IBOutlet weak var emailTextField: MDCOutlinedTextField!
	@IBOutlet weak var textView: UITextView!
	
	override func viewDidLoad() {
        super.viewDidLoad()
		setupViews()
    }
	
	private func setupViews() {
		UIHelper.prepareTextFields(textFields: [emailTextField])
		emailTextField.placeholder = "E-Mail для обратной связи"
		emailTextField.label.text = "E-Mail"
		textView.layer.cornerRadius = 4
		textView.layer.borderWidth = 1
		textView.layer.borderColor = Colors.accentWeak.cgColor
	}
	
	@IBAction func sendPressed(_ sender: Any) {
		let email = emailTextField.text ?? ""
		let text = textView.text ?? ""
		let request = FeedbackRequest(email: email, text: text)
		NetworkManager.shared.postFeedback(feedback: request) { [weak self] _ in
			self?.showAlert("Обращение успешно отправлено!")
			self?.emailTextField.text = ""
			self?.textView.text = ""
		}
	}
}
