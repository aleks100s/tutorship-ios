//
//  ProfileRequest.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Foundation

class ProfileRequest: Encodable, BaseModel {
	internal init(fcmToken: String) {
		self.fcmToken = fcmToken
	}
	
	var fcmToken: String
}
