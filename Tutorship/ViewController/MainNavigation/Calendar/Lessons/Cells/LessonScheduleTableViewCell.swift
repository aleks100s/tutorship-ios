//
//  LessonScheduleTableViewCell.swift
//  Tutorship
//
//  Created by Alexander on 09.07.2021.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextFields

protocol LessonScheduleTableViewCellDelegate: AnyObject {
	func scheduleTypeChanged(_ type: ScheduleType)
	func endDatePicked(_ date: Date)
	func pickWeekdays()
}

class LessonScheduleTableViewCell: UITableViewCell {
	
	@IBOutlet weak var cardView: UIView!
	@IBOutlet weak var toDateTextField: MDCOutlinedTextField!
	@IBOutlet weak var weekdaysButton: UIButton!
	@IBOutlet weak var scheduleSelector: UISegmentedControl!
	
	var startDate: Date = Date() {
		didSet {
			datePicker.minimumDate = startDate
		}
	}
	var weekdays: [Weekday] = [] {
		didSet {
			weekdaysButton.isHidden = weekdays.isEmpty
			let title = weekdays.map({$0.rawValue}).joined(separator: ", ")
			weekdaysButton.setTitle(title, for: .normal)
		}
	}
	weak var delegate: LessonScheduleTableViewCellDelegate?
	
	private var datePicker: UIDatePicker = {
		let datePicker = UIDatePicker()
		datePicker.calendar = Calendar.current
		datePicker.locale = Locale(identifier: "ru_RU")
		datePicker.datePickerMode = .date
		if #available(iOS 13.4, *) {
			datePicker.preferredDatePickerStyle = .wheels
		}
		return datePicker
	}()
	
    override func awakeFromNib() {
        super.awakeFromNib()
		cardView.layer.cornerRadius = 20
		var titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
		UISegmentedControl.appearance().setTitleTextAttributes(titleTextAttributes, for: .normal)
		toDateTextField.inputView = datePicker
		UIHelper.prepareTextFields(textFields: [toDateTextField])
		datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
		datePicker.minimumDate = startDate
		scheduleSelector.addTarget(self, action: #selector(scheduleTypeChanged(_:)), for: .valueChanged)
    }
	
	@IBAction func scheduleTypeChanged(_ sender: UISegmentedControl) {
		var type = ScheduleType.workDays
		switch sender.selectedSegmentIndex {
		case 0:
			type = .workDays
		case 1:
			type = .everyday
		case 2:
			type = .everyWeek
			delegate?.pickWeekdays()
		default:
			type = .workDays
		}
		delegate?.scheduleTypeChanged(type)
	}
	
	@IBAction func weekdaysPressed(_ sender: Any) {
		delegate?.pickWeekdays()
	}
	
	@objc func dateChanged(_ sender: UIDatePicker) {
		let date = sender.date
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "dd MMMM yyyy"
		dateFormatter.locale = Locale(identifier: "ru_RU")
		toDateTextField.text = dateFormatter.string(from: date)
		delegate?.endDatePicked(date)
	}
}

enum ScheduleType {
	case everyday
	case everyWeek
	case workDays
}
