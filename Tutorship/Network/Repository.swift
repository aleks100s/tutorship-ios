//
//  Repository.swift
//  Tutorship
//
//  Created by Alexander on 22.05.2021.
//

import Foundation

class Repository {
	
	static let shared = Repository()
	
	private init() {}
	
	func getProfileData(completion: @escaping (UserResponse) -> Void) {
		NetworkManager.shared.getProfileInfo { response in
			completion(response)
		}
	}
	
	func getSubjectsData(completion: @escaping ([Subject]) -> Void) {
		NetworkManager.shared.getAllSubjects { subjects in
			completion(subjects)
		}
	}
	
	func getStudentsData(completion: @escaping ([Student]) -> Void) {
		NetworkManager.shared.getAllStudents(includeArchived: false) { students in
			completion(students)
		}
	}
	
	func createStudent(student: Student, completion: @escaping (Student) -> Void) {
		NetworkManager.shared.createStudent(student: student) { student in
			completion(student)
		}
	}
	
	func updateStudent(student: Student, completion: @escaping (Student) -> Void) {
		NetworkManager.shared.updateStudent(student: student) { student in
			completion(student)
		}
	}
	
	func deleteStudent(studentID: Int) {
		NetworkManager.shared.deleteStudent(studentID: studentID) {_ in}
	}
	
	func getGroupsData(completion: @escaping ([Group]) -> Void) {
		NetworkManager.shared.getAllGroups(includeIndividual: false) { groups in
			completion(groups)
		}
	}
	
	func getAllGroups(completion: @escaping ([Group]) -> Void) {
		NetworkManager.shared.getAllGroups(includeIndividual: true) { groups in
			completion(groups)
		}
	}
	
	func createGroup(group: Group, completion: @escaping (Group) -> Void) {
		NetworkManager.shared.createGroup(group: group) { group in
			completion(group)
		}
	}
	
	func updateGroup(group: Group, completion: @escaping (Group) -> Void) {
		NetworkManager.shared.updateGroup(group: group) { group in
			completion(group)
		}
	}
	
	func deleteGroup(groupID: Int) {
		NetworkManager.shared.deleteGroup(groupID: groupID) {_ in}
	}
	
	func getGroupStudentsData(groupID: Int, completion: @escaping ([Student]) -> Void) {
		NetworkManager.shared.getStudentsForGroup(groupID: groupID) { students in
			completion(students)
		}
	}
	
	func createLink(link: Link) {
		NetworkManager.shared.createLinkAndProgress(link: link) { link in
			print(link)
		}
	}
	
	func deleteLink(link: Link) {
		NetworkManager.shared.deleteLink(groupID: link.groupID, studentID: link.studentID) { result in
			print(result)
		}
	}
	
	func getLessons(dateFrom: Double = 0, dateTo: Double = 0, after: Double = 0, groupID: Int = 0, completion: @escaping ([FullLesson]) -> Void) {
		NetworkManager.shared.getLessons(dateFrom: dateFrom, dateTo: dateTo, after: after, groupID: groupID) { lessons in
			completion(lessons)
		}
	}
	
	func getTransaction(last: Int = 0, completion: @escaping ([ComplexTransaction]) -> Void) {
		NetworkManager.shared.getTransactions(last: last) { transactions in
			completion(transactions)
		}
	}
	
	func getAttendancePercent(start: Double, end: Double, groupID: Int, completion: @escaping (Double) -> Void) {
		NetworkManager.shared.getAttendancePercent(start: start, end: end, groupID: groupID) { response in
			completion(response.attendancePercent)
		}
	}
	
	func getTotalIncome(start: Double, end: Double, groupID: Int, completion: @escaping (Double) -> Void) {
		NetworkManager.shared.getTotalIncome(start: start, end: end, groupID: groupID) { response in
			completion(response.totalIncome)
		}
	}
	
	func getLessonsCount(start: Double, end: Double, groupID: Int, completion: @escaping (Int) -> Void) {
		NetworkManager.shared.getLessonsCount(start: start, end: end, groupID: groupID) { response in
			completion(response.lessonsCount)
		}
	}
	
	func getExpiredTasks(completion: @escaping ([ComplexTask]) -> Void) {
		NetworkManager.shared.getExpiredTasks { tasks in
			completion(tasks)
		}
	}
	
	func getExpiredPayments(completion: @escaping ([FullAttendance]) -> Void) {
		NetworkManager.shared.getDebtors { attendances in
			completion(attendances)
		}
	}
	
	func getAttendances(for groupID: Int, lessonID: Int, completion: @escaping ([FullAttendance]) -> Void) {
		NetworkManager.shared.getAttendances(groupID: groupID, lessonID: lessonID) { attendances in
			completion(attendances)
		}
	}
	
	func markAttendancePaid(transaction: Transaction, completion: @escaping (Transaction) -> Void) {
		NetworkManager.shared.payForLesson(transaction: transaction) { transaction in
			completion(transaction)
		}
	}
	
	func updateAttendance(attendance: Attendance) {
		NetworkManager.shared.updateAttendance(attendance: attendance) { _ in
			//
		}
	}
	
	func updateTask(task: Task, completion: @escaping () -> Void) {
		NetworkManager.shared.updateTask(task: task) { _ in
			completion()
		}
	}
	
	func getTopics(subjectID: Int, completion: @escaping ([String]) -> Void) {
		NetworkManager.shared.getSubjectTopics(subjectID: subjectID) { response in
			completion(response.topics)
		}
	}
	
	func createLesson(lesson: Lesson, completion: @escaping () -> Void) {
		NetworkManager.shared.createLesson(lesson: lesson) { lesson in
			completion()
		}
	}
	
	func deleteLesson(id: Int, completion: @escaping () -> Void) {
		NetworkManager.shared.deleteLesson(lessonID: id) { _ in
			completion()
		}
	}
	
	func getDiscount(groupID: Int, studentID: Int, completion: @escaping (Int) -> Void) {
		NetworkManager.shared.getStudentDiscount(groupID: groupID, studentID: studentID) { response in
			completion(response.discount)
		}
	}
	
	func finishLesson(lessonID: Int, request: FinishLessonRequest, completion: @escaping () -> Void) {
		NetworkManager.shared.finishLesson(lessonID: lessonID, request: request) { _ in
			completion()
		}
	}
	
	func checkHomework(lessonID: Int, completion: @escaping (Bool) -> Void) {
		NetworkManager.shared.lessonHasHomework(lessonID: lessonID) { response in
			completion(response.hasHomework)
		}
	}
	
	func createHomework(homework: Homework, completion: @escaping (Homework) -> Void) {
		NetworkManager.shared.createHomework(homework: homework) { homework in
			completion(homework)
		}
	}
	
	func loadHomework(groupID: Int, lessonID: Int, completion: @escaping (FullHomework) -> Void) {
		NetworkManager.shared.getHomework(groupID: groupID, lessonID: lessonID) { homework in
			if homework.count > 0 {
				completion(homework[0])
			}
		}
	}
}
