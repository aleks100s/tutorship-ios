//
//  ExpiredHomeworkCollectionViewCell.swift
//  Tutorship
//
//  Created by Alexander on 03.06.2021.
//

import UIKit

class ExpiredHomeworkCollectionViewCell: UICollectionViewCell {
	
	@IBOutlet weak var cardView: UIView!
	@IBOutlet weak var studentLabel: UILabel!
	@IBOutlet weak var topicLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!
	@IBOutlet weak var statusLabel: UILabel!
	
	var viewModel: TaskViewModel? {
		didSet {
			viewModel?.studentName.bind(listener: { value in
				self.studentLabel.text = value
			})
			viewModel?.lessonTopic.bind(listener: { value in
				self.topicLabel.text = value
			})
			viewModel?.deadline.bind(listener: { value in
				self.dateLabel.text = value
			})
			viewModel?.taskStatus.bind(listener: { value in
				self.statusLabel.text = value
			})
		}
	}
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		cardView.layer.cornerRadius = 8
		UIHelper.setViewShadow([cardView])
    }

}
