//
//  GroupTableViewCell.swift
//  Tutorship
//
//  Created by Alexander on 26.05.2021.
//

import UIKit

class GroupTableViewCell: UITableViewCell {

	@IBOutlet weak var initialsView: InitialsView!
	@IBOutlet weak var groupNameLabel: UILabel!
	
	var viewModel: GroupViewModel? {
		didSet {
			bindViewModel()
		}
	}
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
		self.backgroundColor = selected ? Colors.accentWeak : .clear
    }
    
	func bindViewModel() {
		viewModel?.initials.bind { value in
			self.initialsView.initialsLabel.text = value
		}
		viewModel?.color.bind(listener: { color in
			self.initialsView.roundView.backgroundColor = color
		})
		viewModel?.groupName.bind { value in
			self.groupNameLabel.text = value
		}
	}
}
