//
//  AttendancePercentResponse.swift
//  Tutorship
//
//  Created by Alexander on 18.05.2021.
//

import Foundation

class AttendancePercentResponse: Decodable, BaseModel {
	var attendancePercent: Double
	
	enum CodingKeys: String, CodingKey {
		case attendancePercent = "attendance_percent"
	}
}
