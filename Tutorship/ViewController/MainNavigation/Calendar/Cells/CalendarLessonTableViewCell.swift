//
//  CalendarLessonTableViewCell.swift
//  Tutorship
//
//  Created by Alexander on 11.07.2021.
//

import UIKit

class CalendarLessonTableViewCell: UITableViewCell {

	@IBOutlet weak var timeLabel: UILabel!
	@IBOutlet weak var indicator: UIView!
	@IBOutlet weak var groupNameLabel: UILabel!
	@IBOutlet weak var topicLabel: UILabel!
	@IBOutlet weak var cardView: UIView!
	
	var lesson: LessonViewModel? {
		didSet {
			lesson?.lessonTimeStart.bind(listener: { value in
				self.timeLabel.text = value
			})
			lesson?.groupColor.bind(listener: { color in
				self.indicator.backgroundColor = color
			})
			lesson?.groupName.bind(listener: { name in
				self.groupNameLabel.text = name
			})
			lesson?.lessonTopic.bind(listener: { topic in
				self.topicLabel.text = topic
			})
		}
	}
	
	override func awakeFromNib() {
        super.awakeFromNib()
		indicator.layer.cornerRadius = 10
		cardView.layer.cornerRadius = 16
    }
}
