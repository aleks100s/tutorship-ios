//
//  ExpiredAttendanceCollectionViewCell.swift
//  Tutorship
//
//  Created by Alexander on 03.06.2021.
//

import UIKit

class AttendanceCollectionViewCell: UICollectionViewCell {

	@IBOutlet weak var cardView: UIView!
	@IBOutlet weak var studentLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!
	@IBOutlet weak var priceLabel: UILabel!
	
	var viewModel: AttendanceViewModel? {
		didSet {
			viewModel?.studentName.bind { value in
				self.studentLabel.text = value
			}
			viewModel?.lessonDate.bind { value in
				self.dateLabel.text = value
			}
			viewModel?.lessonPrice.bind { value in
				self.priceLabel.text = value
			}
		}
	}
	
	override func awakeFromNib() {
        super.awakeFromNib()
		cardView.layer.cornerRadius = 8
		UIHelper.setViewShadow([cardView])
    }

}
