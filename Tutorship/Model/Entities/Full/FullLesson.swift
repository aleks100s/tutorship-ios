//
//  FullLesson.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Foundation

class FullLesson: Codable, BaseModel {
	var lesson: Lesson?
	var group: Group?
	
	required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		lesson = try container.decode(Lesson.self, forKey: .lesson)
		group = try container.decode(Group.self, forKey: .group)
	}
	
	enum CodingKeys: String, CodingKey {
		case lesson = "lesson_data"
		case group = "group_data"
	}
}
