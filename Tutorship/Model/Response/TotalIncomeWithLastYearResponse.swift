//
//  TotalIncomeWithLastYearResponse.swift
//  Tutorship
//
//  Created by Alexander on 18.05.2021.
//

import Foundation

class TotalIncomeWithLastYearResponse: Decodable, BaseModel {
	var lastYear: Double
	var thisYear: Double
	
	enum CodingKeys: String, CodingKey {
		case lastYear = "last_year"
		case thisYear = "this_year"
	}
}
