//
//  Subject.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Foundation

class Subject: Codable, BaseModel {
	var id: Int = 0
	var name: String = ""
	
	init(json: JSON) {
		if let id = json["id"] as? Int {
			self.id = id
		}
		if let name = json["name"] as? String {
			self.name = name
		}
	}
}
