//
//  AttendanceViewController.swift
//  Tutorship
//
//  Created by Alexander on 16.06.2021.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class AttendanceViewController: BaseViewController {
	override class func controller() -> Self {
		return AttendanceViewController(nibName: String(describing: Self.self), bundle: nil) as! Self
	}
	
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!
	@IBOutlet weak var priceLabel: UILabel!
	@IBOutlet weak var attendanceImageView: UIImageView!
	@IBOutlet weak var paymentImageView: UIImageView!
	@IBOutlet weak var markTextField: MDCOutlinedTextField!
	@IBOutlet weak var commentTextField: MDCOutlinedTextField!
	@IBOutlet weak var markAsPaidButton: UIButton!
	
	var attendance: AttendanceViewModel?
	
	override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
		displayAttendance()
    }
	
	override func viewWillDisappear(_ animated: Bool) {
		saveChanges()
	}
	
	private func setupViews() {
		UIHelper.prepareTextFields(textFields: [markTextField, commentTextField])
		markTextField.label.text = "Оценка за занятие"
		commentTextField.label.text = "Комментарий"
	}
	
	private func displayAttendance() {
		attendance?.studentName.bind { value in
			self.nameLabel.text = value
		}
		attendance?.lessonDate.bind { value in
			self.dateLabel.text = value
		}
		attendance?.lessonPrice.bind { value in
			self.priceLabel.text = value
		}
		attendance?.studentAbsent.bind { isAbsent in
			self.attendanceImageView.image = UIImage(systemName: isAbsent ? "xmark" : "checkmark")
			self.attendanceImageView.tintColor = isAbsent ? .systemRed : .systemGreen
		}
		attendance?.attendancePaid.bind { isPaid in
			self.paymentImageView.image = UIImage(systemName: isPaid ? "checkmark" : "xmark")
			self.paymentImageView.tintColor = isPaid ? .systemGreen : .systemRed
			self.markAsPaidButton.isHidden = isPaid
		}
		attendance?.studentMark.bind { value in
			self.markTextField.text = value > 0 ? "\(value)" : ""
		}
		attendance?.attendanceComment.bind { value in
			self.commentTextField.text = value
		}
	}
	
	private func saveChanges() {
		let mark = Int(markTextField.text ?? "0") ?? 0
		let comment = commentTextField.text ?? ""
		attendance?.updateAttendance(mark: mark, comment: comment)
	}

	@IBAction func markAsPaid(_ sender: Any) {
		attendance?.markAsPaid()
	}
}
