//
//  TopicCollectionViewCell.swift
//  Tutorship
//
//  Created by Alexander on 17.07.2021.
//

import UIKit

protocol TopicCollectionViewCellDelegate: AnyObject {
	func topicRemoved(_ topic: String?)
}

class TopicCollectionViewCell: UICollectionViewCell {

	@IBOutlet weak var mainView: UIView!
	@IBOutlet weak var topicLabel: UILabel!
	
	weak var delegate: TopicCollectionViewCellDelegate?
	
	var topic: String? {
		didSet {
			self.topicLabel.text = topic
		}
	}
	
	override func awakeFromNib() {
        super.awakeFromNib()
		mainView.layer.cornerRadius = 18
    }

	@IBAction func deletePressed(_ sender: Any) {
		delegate?.topicRemoved(topic)
	}
}
