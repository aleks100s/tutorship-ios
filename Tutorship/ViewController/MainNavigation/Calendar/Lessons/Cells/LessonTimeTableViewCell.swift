//
//  LessonTimeTableViewCell.swift
//  Tutorship
//
//  Created by Alexander on 09.07.2021.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextFields

protocol LessonTimeTableViewCellDelegate: AnyObject {
	func datePicked(_ date: Date)
	func timePicked(_ date: Date)
	func durationPicked(_ duration: Int)
}

class LessonTimeTableViewCell: UITableViewCell {

	@IBOutlet weak var dateTextField: MDCOutlinedTextField!
	@IBOutlet weak var timeTextField: MDCOutlinedTextField!
	@IBOutlet weak var durationTextField: MDCOutlinedTextField!
	@IBOutlet weak var cardView: UIView!
	
	weak var delegate: LessonTimeTableViewCellDelegate?
	
	var isEnabled: Bool = true {
		didSet {
			dateTextField.isEnabled = isEnabled
			timeTextField.isEnabled = isEnabled
			durationTextField.isEnabled = isEnabled
		}
	}
	
	private var datePicker: UIDatePicker = {
		let datePicker = UIDatePicker()
		datePicker.calendar = Calendar.current
		datePicker.locale = Locale(identifier: "ru_RU")
		datePicker.datePickerMode = .date
		if #available(iOS 13.4, *) {
			datePicker.preferredDatePickerStyle = .wheels
		}
		datePicker.minimumDate = Date()
		return datePicker
	}()
	
	private var timePicker: UIDatePicker = {
		let datePicker = UIDatePicker()
		datePicker.calendar = Calendar.current
		datePicker.locale = Locale(identifier: "ru_RU")
		datePicker.datePickerMode = .time
		if #available(iOS 13.4, *) {
			datePicker.preferredDatePickerStyle = .wheels
		}
		return datePicker
	}()
	
	private var durationPicker: UIPickerView = {
		let picker = UIPickerView()
		return picker
	}()
	
	override func awakeFromNib() {
        super.awakeFromNib()
		cardView.layer.cornerRadius = 20
		UIHelper.prepareTextFields(textFields: [dateTextField, timeTextField, durationTextField])
		dateTextField.label.text = "Выбрать дату"
		timeTextField.label.text = "Выбрать время"
		durationTextField.label.text = "Длительность"
		
		dateTextField.inputView = datePicker
		timeTextField.inputView = timePicker
		datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
		timePicker.addTarget(self, action: #selector(timeChanged(_:)), for: .valueChanged)
		
		durationPicker.dataSource = self
		durationPicker.delegate = self
		durationTextField.inputView = durationPicker
		
		datePicker.date = Date()
		timePicker.date = Date()
    }
	
	@objc func dateChanged(_ sender: UIDatePicker) {
		let date = sender.date
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "dd MMMM yyyy"
		dateFormatter.locale = Locale(identifier: "ru_RU")
		dateTextField.text = dateFormatter.string(from: date)
		delegate?.datePicked(date)
	}
	
	@objc func timeChanged(_ sender: UIDatePicker) {
		let date = sender.date
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "HH:mm"
		timeTextField.text = dateFormatter.string(from: date)
		delegate?.timePicked(date)
	}
}

extension LessonTimeTableViewCell: UIPickerViewDataSource, UIPickerViewDelegate {
	func numberOfComponents(in pickerView: UIPickerView) -> Int {
		1
	}
	
	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		10
	}
	
	func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
		"\(row * 10)"
	}
	
	func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
		durationTextField.text = "\(row * 10)"
		delegate?.durationPicked(row * 10)
	}
}
