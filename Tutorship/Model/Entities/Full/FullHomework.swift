//
//  FullHomework.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Foundation

class FullHomework: Codable, BaseModel {
	var homework: Homework?
	var lesson: Lesson?
	
	required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		homework = try container.decode(Homework.self, forKey: .homework)
		lesson = try container.decode(Lesson.self, forKey: .lesson)
	}
	
	enum CodingKeys: String, CodingKey {
		case homework = "homework_data"
		case lesson = "lesson_data"
	}
}
