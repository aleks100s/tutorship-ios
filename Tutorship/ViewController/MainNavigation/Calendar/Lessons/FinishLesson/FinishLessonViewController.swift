//
//  FinishLessonViewController.swift
//  Tutorship
//
//  Created by Alexander on 20.07.2021.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class FinishLessonViewController: UIViewController {
	
	class func controller() -> Self {
		return FinishLessonViewController(nibName: String(describing: Self.self), bundle: nil) as! Self
	}
	
	@IBOutlet weak var groupTextField: MDCOutlinedTextField!
	@IBOutlet weak var topicTextField: MDCOutlinedTextField!
	@IBOutlet weak var priceTextField: MDCOutlinedTextField!
	@IBOutlet weak var commentView: UIView!
	@IBOutlet weak var attendanceView: UIView!
	@IBOutlet weak var commentTextField: MDCOutlinedTextField!
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var tableViewHeight: NSLayoutConstraint!
	@IBOutlet weak var homeworkButton: UIButton!
	
	var lesson: LessonViewModel?
	var students: [(StudentViewModel, Int)] = []
	var attendances: [AttendanceViewModel] = [] {
		didSet {
			tableView.allowsSelection = true
			tableView.reloadData()			
		}
	}
	
	override func viewDidLoad() {
        super.viewDidLoad()
		setupViews()
		setupTableView()
		displayLesson()
		loadStudents()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		if lesson?.lessonFinished.value == true {
			prepareFinishedLesson()
		} else {
			title = "Завершить занятие"
			navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Завершить", style: .done, target: self, action: #selector(finishLesson))
		}
	}
	
	private func prepareFinishedLesson() {
		lesson?.lessonDate.bind(listener: { date in
			self.title = date
		})
		loadAttendances()
		checkHomework()
	}
	
	override func viewDidLayoutSubviews() {
		//let h = tableView.contentSize.height
		//tableViewHeight.constant = tableView.contentSize.height
//		if lesson?.lessonFinished.value == true {
//			tableViewHeight.constant = CGFloat(188 * attendances.count)
//		} else {
//			tableViewHeight.constant = CGFloat(188 * students.count)
//		}
	}
	
	private func setupViews() {
		UIHelper.prepareTextFields(textFields: [groupTextField, topicTextField, priceTextField, commentTextField])
		commentView.layer.cornerRadius = 12
		attendanceView.layer.cornerRadius = 12
		lesson?.lessonFinished.bind(listener: { isFinished in
			self.commentTextField.isEnabled = !isFinished
			self.tableView.reloadData()
		})
	}
	
	private func setupTableView() {
		tableView.estimatedRowHeight = UITableView.automaticDimension
		tableView.allowsSelection = false
		tableView.register(UINib(nibName: "FillAttendanceTableViewCell", bundle: nil), forCellReuseIdentifier: "FillAttendanceTableViewCell")
		tableView.dataSource = self
		tableView.delegate = self
	}
	
	private func displayLesson() {
		lesson?.groupName.bind(listener: { name in
			self.groupTextField.text = name
		})
		lesson?.lessonTopic.bind(listener: { topic in
			self.topicTextField.text = topic
		})
		lesson?.lessonPrice.bind(listener: { price in
			self.priceTextField.text = price
		})
		lesson?.lessonComment.bind(listener: { comment in
			self.commentTextField.text = comment
		})
	}
	
	private func loadStudents() {
		students.removeAll()
		GroupViewModel.getGroupStudents(groupID: lesson?.groupID.value ?? 0) { [weak self] students in
			self?.processStudents(students)
		}
	}
	
	private func processStudents(_ students: [StudentViewModel]) {
		if lesson?.lessonFinished.value == true {
			self.students.append(contentsOf: students.map({ student in
				(student, 0)
			}))
		} else {
			students.forEach { student in
				Repository.shared.getDiscount(groupID: lesson?.groupID.value ?? 0, studentID: student.id.value) { discount in
					self.students.append((student, discount))
					if students.count == students.count {
						self.tableView.reloadData()
					}
				}
			}
		}
	}
	
	@objc func finishLesson() {
		showDestructiveDialog(title: "Завершить занятие?", message: "Убедитесь, что все присутствующие были отмечены", buttonTitle: "Завершить?") {
			self.performFinishing()
		}
	}
	
	private func performFinishing() {
		var attendances: [Attendance] = []
		for i in students.indices {
			let student = students[i].0
			let cell = tableView.cellForRow(at: IndexPath(row: i, section: 0)) as! FillAttendanceTableViewCell
			let attendance = Attendance(id: 0, studentID: student.id.value, lessonID: lesson?.lessonID.value ?? 0, isAbsent: !cell.attendanceSwitch.isOn, mark: Int(cell.markTextField.text ?? "") ?? 0, price: Int(cell.priceTextField.text ?? "") ?? 0, isPaid: false, comment: cell.commentTextField.text ?? "")
			attendances.append(attendance)
		}
		let request = FinishLessonRequest(attendances: attendances)
		Repository.shared.finishLesson(lessonID: lesson?.lessonID.value ?? 0, request: request) {
			self.lesson?.lessonFinished.value = true
			self.showAlert(title: "Урок успешно завершен", message: nil) {
				self.prepareFinishedLesson()
			}
		}
	}
	
	private func loadAttendances() {
		AttendanceViewModel.getAttendances(for: lesson?.groupID.value ?? 0, lessonID: lesson?.lessonID.value ?? 0) { [weak self] attendances in
			self?.attendances = attendances
		}
	}
	
	private func checkHomework() {
		Repository.shared.checkHomework(lessonID: lesson?.lessonID.value ?? 0) { hasHomework in
			self.showHomeworkButton(hasHomework)
		}
	}
	
	private func showHomeworkButton(_ hasHomework: Bool) {
		homeworkButton.isHidden = false
		if hasHomework {
			homeworkButton.setTitle("Открыть домашнее задание", for: .normal)
			homeworkButton.addTarget(self, action: #selector(openHomework), for: .touchUpInside)
		} else {
			homeworkButton.setTitle("Задать домашнее задание", for: .normal)
			homeworkButton.addTarget(self, action: #selector(createHomework), for: .touchUpInside)
		}
	}
	
	@objc func openHomework() {
		let controller = HomeworkViewController.controller()
		controller.contentMode = .simple
		controller.lessonID = lesson?.lessonID.value
		controller.groupID = lesson?.groupID.value
		navigationController?.pushViewController(controller, animated: true)
	}
	
	@objc func createHomework() {
		let controller = HomeworkViewController.controller()
		controller.contentMode = .creating
		controller.lessonID = lesson?.lessonID.value
		controller.groupID = lesson?.groupID.value
		navigationController?.pushViewController(controller, animated: true)
	}
}

extension FinishLessonViewController: UITableViewDataSource, UITableViewDelegate {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		students.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "FillAttendanceTableViewCell", for: indexPath) as! FillAttendanceTableViewCell
		if lesson?.lessonFinished.value == true {
			guard attendances.count > indexPath.row else {
				return UITableViewCell()
			}
			cell.attendance = attendances[indexPath.row]
			tableViewHeight.constant = CGFloat(192 * attendances.count)
		} else {
			let student = students[indexPath.row].0
			cell.student = student
			let discountPercent = students[indexPath.row].1
			let price = Float(100 - discountPercent) * (Float(lesson?.lessonPriceInt.value ?? 0)) / 100
			cell.priceTextField.text = "\(Int(price))"
			tableViewHeight.constant = CGFloat(192 * students.count)
		}
		return cell
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		let attendance = attendances[indexPath.row]
		if attendance.attendancePaid.value == false {
			showDestructiveDialog(title: "Отметить посещение оплаченным?", buttonTitle: "Оплачено") {
				attendance.markAsPaid()
			}
		}
	}
}
