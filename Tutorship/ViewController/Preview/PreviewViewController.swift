//
//  PreviewViewController.swift
//  Tutorship
//
//  Created by Alexander on 22.05.2021.
//

import UIKit

class PreviewViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
	@IBAction func continuePressed(_ sender: Any) {
		NotificationCenter.default.post(name: Notifications.previewWatched.name, object: nil)
	}
}
