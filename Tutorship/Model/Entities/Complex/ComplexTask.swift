//
//  ComplexTask.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Foundation

class ComplexTask: Codable, BaseModel {
	var id: Int = 0
	var homeworkID: Int = 0
	var passedAt: Date = Date()
	var deadline: Date = Date()
	var studentID: Int = 0
	var mark: Int = 0
	var studentName: String = ""
	var lessonTopic: String = ""
	var comment: String = ""
	var homeworkContent: String = ""
	var createdAt: Date = Date()
	var isPassed: Bool = false

	
	required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		id = try container.decode(Int.self, forKey: .id)
		homeworkID = try container.decode(Int.self, forKey: .homeworkID)
		let passedAt = try container.decode(Double.self, forKey: .passedAt)
		self.isPassed = passedAt > 0
		self.passedAt = Date(timeIntervalSince1970: passedAt / 1000)
		let deadline = try container.decode(Double.self, forKey: .deadline)
		self.deadline = Date(timeIntervalSince1970: deadline / 1000)
		self.studentID = try container.decode(Int.self, forKey: .studentID)
		mark = try container.decode(Int.self, forKey: .mark)
		studentName = try container.decode(String.self, forKey: .studentName)
		lessonTopic = try container.decode(String.self, forKey: .lessonTopic)
		comment = try container.decode(String.self, forKey: .comment)
		homeworkContent = try container.decode(String.self, forKey: .homeworkContent)
		let createdAt = try container.decode(Double.self, forKey: .createdAt)
		self.createdAt = Date(timeIntervalSince1970: createdAt / 1000)
	}
	
	func getStatus() -> String {
		if passedAt.timeIntervalSince1970 != 0 {
			if deadline < Date() {
				return "ПРОСРОЧЕНО"
			} else {
				return "ЗАДАНО"
			}
		} else {
			if passedAt < deadline {
				return "СДАНО"
			} else {
				return "С ОПОЗДАНИЕМ"
			}
		}
	}
	
	enum CodingKeys: String, CodingKey {
		case id
		case homeworkID = "homework"
		case passedAt = "passed"
		case deadline
		case studentID = "student"
		case mark
		case studentName = "fullName"
		case lessonTopic = "topic"
		case comment
		case homeworkContent = "content"
		case createdAt
	}
}
