//
//  Homework.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Foundation

class Homework: Codable, BaseModel {
	var id: Int = 0
	var lessonID: Int = 0
	var content: String = ""
	var comment: String = ""
	var deadline: Date = Date()
	var tutor: Int = 0
	
	init(lessonID: Int, content: String, comment: String, deadline: Date) {
		self.lessonID = lessonID
		self.content = content
		self.comment = comment
		self.deadline = deadline
	}
	
	required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		id  = try container.decode(Int.self, forKey: .id)
		lessonID = try container.decode(Int.self, forKey: .lessonID)
		content = try container.decode(String.self, forKey: .content)
		comment = try container.decode(String.self, forKey: .comment)
		let deadline = try container.decode(Double.self, forKey: .deadline)
		self.deadline = Date(timeIntervalSince1970: deadline / 1000)
		//tutor = try container.decode(Int.self, forKey: .tutor)
	}
	
	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(id, forKey: .id)
		try container.encode((deadline.timeIntervalSince1970 * 1000).rounded(), forKey: .deadline)
		try container.encode(comment, forKey: .comment)
		try container.encode(lessonID, forKey: .lessonID)
		try container.encode(content, forKey: .content)
		try container.encode(Defaults.userID, forKey: .tutor)
	}
	
	enum CodingKeys: String, CodingKey {
		case id
		case lessonID = "lesson"
		case content
		case comment
		case deadline
		case tutor
	}
}
