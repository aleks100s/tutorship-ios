//
//  AuthResponse.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Foundation

class AuthResponse: Decodable, BaseModel {
	var token: String
	var email: String
	var userID: Int
	
	enum CodingKeys: String, CodingKey {
		case token
		case email
		case userID = "user_id"
	}
}
