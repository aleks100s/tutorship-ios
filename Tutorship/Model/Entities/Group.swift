//
//  Group.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Foundation
import UIKit.UIColor

class Group: Codable, BaseModel {
	var id: Int = 0
	var name: String = ""
	var subjectID: Int = 0
	var subject: String = ""
	var comment: String = ""
	var isIndividual: Bool = false
	var color: String = "FF000000"
	var isArchived: Bool = false
	var tutor: Int = 0
	
	internal init(id: Int = 0, name: String, subjectID: Int) {
		self.id = id
		self.name = name
		self.subjectID = subjectID
		self.color = UIColor.random().hexString()
	}
	
	required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		id  = try container.decode(Int.self, forKey: .id)
		name = try container.decode(String.self, forKey: .name)
		subjectID = try container.decode(Int.self, forKey: .subjectID)
		subject = (try? container.decode(String.self, forKey: .subject)) ?? ""
		comment = try container.decode(String.self, forKey: .comment)
		isIndividual = try container.decode(Bool.self, forKey: .isIndividual)
		let color = try container.decode(Int.self, forKey: .color)
		self.color = String(format: "%06X", color)
		isArchived = try container.decode(Bool.self, forKey: .isArchived)
	}
	
	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(id, forKey: .id)
		try container.encode(name, forKey: .name)
		try container.encode(subjectID, forKey: .subjectID)
		try container.encode(subject, forKey: .subject)
		try container.encode(comment, forKey: .comment)
		try container.encode(isIndividual, forKey: .isIndividual)
		let color = Int(self.color, radix: 16) ?? 0
		try container.encode(color, forKey: .color)
		try container.encode(isArchived, forKey: .isArchived)
		try container.encode(Defaults.userID, forKey: .tutor)
	}
	
	enum CodingKeys: String, CodingKey {
		case id
		case name
		case subjectID = "subject"
		case subject = "subject_name"
		case comment
		case isIndividual
		case color
		case isArchived
		case tutor
	}
}
