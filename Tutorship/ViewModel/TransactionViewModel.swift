//
//  TransactionViewModel.swift
//  Tutorship
//
//  Created by Alexander on 07.06.2021.
//

import Foundation

class TransactionViewModel {
	static func loadLastTransactions(completion: @escaping ([TransactionViewModel]) -> Void) {
		Repository.shared.getTransaction(last: 3) { transactions in
			let viewModels = transactions.map { transaction in
				TransactionViewModel(transaction: transaction)
			}
			completion(viewModels)
		}
	}
	
	static func loadAllTransactions(completion: @escaping ([TransactionViewModel]) -> Void) {
		Repository.shared.getTransaction { transactions in
			let viewModels = transactions.map { transaction in
				TransactionViewModel(transaction: transaction)
			}
			completion(viewModels)
		}
	}
	
	var transactionID: LiveData<Int> = LiveData(0)
	var attendanceID: LiveData<Int> = LiveData(0)
	var transactionTimestamp: LiveData<String> = LiveData("")
	var transactionAmount: LiveData<String> = LiveData("")
	var transactionStudent: LiveData<String> = LiveData("")
	var transactionLessonID: LiveData<Int> = LiveData(0)
	var lessonDate: LiveData<String> = LiveData("")
	
	init() {}
	
	init(transaction: ComplexTransaction) {
		self.transactionID.value = transaction.id
		self.attendanceID.value = transaction.attendanceID
		self.transactionTimestamp.value = dateFormatter.string(from: transaction.timestamp)
		self.transactionAmount.value = "\(Int(transaction.amount))₽"
		self.transactionStudent.value = transaction.studentName
		self.transactionLessonID.value = transaction.lessonID
		self.lessonDate.value = dateFormatter.string(from: transaction.lessonDate)
	}
	
	var dateFormatter: DateFormatter {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "dd.MM.yyyy"
		dateFormatter.locale = Locale(identifier: "ru_RU")
		return dateFormatter
	}
}
