//
//  RegistrationViewController.swift
//  Tutorship
//
//  Created by Alexander on 22.05.2021.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class RegistrationViewController: BaseViewController {
	override class var storyboardName: String { "Authentication" }
	
	@IBOutlet weak var usernameTextField: MDCOutlinedTextField!
	@IBOutlet weak var emailTextField: MDCOutlinedTextField!
	@IBOutlet weak var passwordTextField: MDCOutlinedTextField!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		prepareViews()
    }
	
	private func prepareViews() {
		UIHelper.prepareTextFields(textFields: [usernameTextField, emailTextField, passwordTextField])
		
		usernameTextField.label.text = "Имя пользователя"
		emailTextField.label.text = "E-Mail"
		passwordTextField.label.text = "Пароль"
		
		usernameTextField.delegate = self
		emailTextField.delegate = self
		passwordTextField.delegate = self
	}
	
	@IBAction func registrationPressed(_ sender: Any) {
		let name = usernameTextField.text ?? ""
		let email = emailTextField.text ?? ""
		let password = passwordTextField.text ?? ""
		
		if !InputValidator.nameValid(name) {
			UIHelper.showErrorInField(usernameTextField, text: "Проверьте введенныe данныe")
			return
		}
		
		if !InputValidator.emailValid(email) {
			UIHelper.showErrorInField(emailTextField, text: "Проверьте введенныe данныe")
			return
		}
		
		if !InputValidator.passwordValid(password) {
			UIHelper.showErrorInField(passwordTextField, text: "Проверьте введенныe данныe")
			return
		}
		
		let request = AuthRequest(password: password, email: email, name: name)
		sendRequest(request)
	}
	
	func sendRequest(_ request: AuthRequest) {
		NetworkManager.shared.register(request: request) { [weak self] response in
			if response {
				self?.processLogin(request: request)
			}
		}
	}
	
	func processLogin(request: AuthRequest) {
		NetworkManager.shared.authorize(request: request) { response in
			Notifications.userAuthorized.post(with: response)
		}
	}
}
