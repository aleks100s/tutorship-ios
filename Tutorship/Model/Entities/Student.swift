//
//  Student.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Foundation
import UIKit.UIColor

class Student: Codable, BaseModel {
	internal init(id: Int = 0, fullName: String = "", school: String = "", grade: Int = 0, phone: String = "", color: String = "#FFFFFFFF", parentName: String = "", parentPhone: String = "", parentEmail: String = "", isArchived: Bool = false) {
		self.id = id
		self.fullName = fullName
		self.school = school
		self.grade = grade
		self.phone = phone
		self.color = color
		self.parentName = parentName
		self.parentPhone = parentPhone
		self.parentEmail = parentEmail
		self.isArchived = isArchived
		self.color = UIColor.random().hexString()
	}
	
	var id: Int = 0
	var fullName: String = ""
	var school: String = ""
	var grade: Int = 0
	var phone: String = ""
	var color: String = "FFFFFFFF"
	var parentName: String = ""
	var parentPhone: String = ""
	var parentEmail: String = ""
	var isArchived: Bool = false
	var tutorID: Int = 0
	
	required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		id = try container.decode(Int.self, forKey: .id)
		fullName = try container.decode(String.self, forKey: .fullName)
		school = try container.decode(String.self, forKey: .school)
		grade = try container.decode(Int.self, forKey: .grade)
		phone = try container.decode(String.self, forKey: .phone)
		let color = try container.decode(Int.self, forKey: .color)
		self.color = String(format: "%06X", color)
		parentName = try container.decode(String.self, forKey: .parentName)
		parentPhone = try container.decode(String.self, forKey: .parentPhone)
		parentEmail = try container.decode(String.self, forKey: .parentEmail)
		isArchived = try container.decode(Bool.self, forKey: .isArchived)
	}
	
	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(id, forKey: .id)
		try container.encode(fullName, forKey: .fullName)
		try container.encode(school, forKey: .school)
		try container.encode(grade, forKey: .grade)
		try container.encode(phone, forKey: .phone)
		let color = Int(self.color, radix: 16) ?? 0
		try container.encode(color, forKey: .color)
		try container.encode(parentName, forKey: .parentName)
		try container.encode(parentPhone, forKey: .parentPhone)
		try container.encode(parentEmail, forKey: .parentEmail)
		try container.encode(isArchived, forKey: .isArchived)
		try container.encode(Defaults.userID ?? 0, forKey: .tutor)
	}
	
	enum CodingKeys: String, CodingKey {
		case id
		case fullName
		case school
		case grade
		case phone
		case color
		case parentName
		case parentPhone
		case parentEmail
		case isArchived
		case tutor
	}
}
