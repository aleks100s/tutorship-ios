//
//  StudentDetailViewController.swift
//  Tutorship
//
//  Created by Alexander on 23.05.2021.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class StudentDetailViewController: BaseViewController {
	override class var storyboardName: String { "Students" }
	
	@IBOutlet weak var initialsView: InitialsView!
	@IBOutlet weak var studentNameTextField: MDCOutlinedTextField!
	@IBOutlet weak var studentPhoneTextField: MDCOutlinedTextField!
	@IBOutlet weak var studingCardView: UIView!
	@IBOutlet weak var schoolTextField: MDCOutlinedTextField!
	@IBOutlet weak var gradeTextField: MDCOutlinedTextField!
	@IBOutlet weak var parentCardView: UIView!
	@IBOutlet weak var parentNameTextField: MDCOutlinedTextField!
	@IBOutlet weak var parentPhoneTextField: MDCOutlinedTextField!
	@IBOutlet weak var parentEmailTextField: MDCOutlinedTextField!
	
	private var datePicker: UIPickerView!
	
	var viewModel = StudentViewModel()
	override var contentMode: BaseViewController.ContentMode {
		didSet {
			if isViewLoaded {
				onContentModeChanged()
			}
		}
	}

    override func viewDidLoad() {
        super.viewDidLoad()
		setupViews()
		bindViewModel()
		onContentModeChanged()
    }
	
	private func setupViews() {
		UIHelper.prepareTextFields(textFields: [studentNameTextField, studentPhoneTextField, schoolTextField, gradeTextField, parentNameTextField, parentPhoneTextField, parentEmailTextField])
		
		parentCardView.layer.cornerRadius = 12
		studingCardView.layer.cornerRadius = 12
		
		studentNameTextField.label.text = "Полное имя"
		studentPhoneTextField.label.text = "Номер телефона"
		schoolTextField.label.text = "Школа"
		gradeTextField.label.text = "Класс"
		parentNameTextField.label.text = "Полное имя"
		parentPhoneTextField.label.text = "Номер телефона"
		parentEmailTextField.label.text = "Электронная почта"
		
		setPickerViewFor(gradeTextField)
	}
	
	private func setPickerViewFor(_ textView: UITextField) {
		datePicker = UIPickerView()
		datePicker.backgroundColor = .white
		datePicker.dataSource = self
		datePicker.delegate = self
		textView.inputView = datePicker
	}
	
	private func bindViewModel() {
		viewModel.initials.bind(listener: { value in
			self.initialsView.initialsLabel.text = value
		})
		viewModel.color.bind(listener: { color in
			self.initialsView.roundView.backgroundColor = color
		})
		viewModel.fullName.bind(listener: { value in
			self.studentNameTextField.text = value
		})
		viewModel.phoneNumber.bind(listener: { value in
			self.studentPhoneTextField.text = value
		})
		viewModel.school.bind(listener: { value in
			self.schoolTextField.text = value
		})
		viewModel.grade.bind(listener: { value in
			self.gradeTextField.text = value
		})
		viewModel.parentName.bind(listener: { value in
			self.parentNameTextField.text = value
		})
		viewModel.parentPhone.bind(listener: { value in
			self.parentPhoneTextField.text = value
		})
		viewModel.parentEmail.bind(listener: { value in
			self.parentEmailTextField.text = value
		})
	}
	
	private func onContentModeChanged() {
		var buttonTitle = "Создать"
		var selector = #selector(saveStudent)
		switch contentMode {
		case .creating:
			title = "Новый ученик"
			buttonTitle = "Создать"
			selector = #selector(saveStudent)
			break
		case .simple:
			title = "Просмотр"
			buttonTitle = "Изменить"
			selector = #selector(editStudent)
			break
		case .editing:
			title = "Редактирование"
			buttonTitle = "Сохранить"
			selector = #selector(updateStudent)
			break
		}
		navigationItem.rightBarButtonItem = UIBarButtonItem(title: buttonTitle, style: .plain, target: self, action: selector)
		let enabled = contentMode != .simple
		studentNameTextField.isEnabled = enabled
		studentPhoneTextField.isEnabled = enabled
		schoolTextField.isEnabled = enabled
		gradeTextField.isEnabled = enabled
		parentNameTextField.isEnabled = enabled
		parentPhoneTextField.isEnabled = enabled
		parentEmailTextField.isEnabled = enabled
	}
	
	@objc func saveStudent() {
		let name = studentNameTextField.text ?? ""
		let phone = studentPhoneTextField.text ?? ""
		let school = schoolTextField.text ?? ""
		let grade = Int(gradeTextField.text?.split(separator: " ")[0] ?? "0") ?? 0
		let parentName = parentNameTextField.text ?? ""
		let parentPhone = parentPhoneTextField.text ?? ""
		let parentEmail = parentEmailTextField.text ?? ""
		
		ActivityIndicatorSheet.addSheet()
		viewModel.create(name: name, phone: phone, school: school, grade: grade, parentName: parentName, parentPhone: parentPhone, parentEmail: parentEmail) {
			ActivityIndicatorSheet.hide()
			self.contentMode = .simple
		}
	}
	
	@objc func editStudent() {
		contentMode = .editing
	}
	
	@objc func updateStudent() {
		let name = studentNameTextField.text ?? ""
		let phone = studentPhoneTextField.text ?? ""
		let school = schoolTextField.text ?? ""
		let grade = Int(gradeTextField.text?.split(separator: " ")[0] ?? "0") ?? 0
		let parentName = parentNameTextField.text ?? ""
		let parentPhone = parentPhoneTextField.text ?? ""
		let parentEmail = parentEmailTextField.text ?? ""
		
		ActivityIndicatorSheet.addSheet()
		viewModel.update(name: name, phone: phone, school: school, grade: grade, parentName: parentName, parentPhone: parentPhone, parentEmail: parentEmail) {
			ActivityIndicatorSheet.hide()
			self.contentMode = .simple
		}
	}
}

extension StudentDetailViewController: UIPickerViewDataSource, UIPickerViewDelegate {
	func numberOfComponents(in pickerView: UIPickerView) -> Int {
		1
	}
	
	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		11
	}
	
	func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
		String(row + 1)
	}
	
	func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
		gradeTextField.text = "\(row + 1) класс"
	}
}
