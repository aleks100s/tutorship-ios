//
//  HasHomeworkResponse.swift
//  Tutorship
//
//  Created by Alexander on 18.05.2021.
//

import Foundation

class HasHomeworkResponse: Decodable, BaseModel {
	var hasHomework: Bool
	
	enum CodingKeys: String, CodingKey {
		case hasHomework = "has_homework"
	}
}
