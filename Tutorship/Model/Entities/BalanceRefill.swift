//
//  BalanceRefill.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Foundation

class BalanceRefill: Codable, BaseModel {
	var id: Int = 0
	var progressID: Int = 0
	var amount: Double = 0
	
	enum CodingKeys: String, CodingKey {
		case id
		case progressID = "progress"
		case amount
	}
}
