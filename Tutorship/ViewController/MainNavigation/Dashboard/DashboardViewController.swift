//
//  DashboardViewController.swift
//  Tutorship
//
//  Created by Alexander on 22.05.2021.
//

import UIKit
import MaterialComponents.MaterialBottomSheet

class DashboardViewController: BaseViewController {
	
	override class var storyboardName: String { "Main" }

	@IBOutlet weak var lessonsCardView: UIView!
	@IBOutlet weak var calendarView: UIView!
	@IBOutlet weak var calendarButton: UIButton!
	@IBOutlet weak var todayLabel: UILabel!
	@IBOutlet weak var lessonsTableView: UITableView!
	@IBOutlet weak var lessonTableViewHeight: NSLayoutConstraint!
	
	@IBOutlet weak var homeworkCollectionView: UICollectionView!
	
	@IBOutlet weak var transactionsView: UIView!
	@IBOutlet weak var transactionsButton: UIButton!
	@IBOutlet weak var transactionsTableView: UITableView!
	@IBOutlet weak var transactionsTableViewHeight: NSLayoutConstraint!
	@IBOutlet weak var transactionsCardView: UIView!
	
	@IBOutlet weak var attendancesCollectionView: UICollectionView!
	
	@IBOutlet weak var statisticsCardView: UIView!
	@IBOutlet weak var statisticsTableView: UITableView!
	@IBOutlet weak var statisticsButton: UIButton!
	@IBOutlet weak var statisticsMonthLabel: UILabel!
	@IBOutlet weak var statisticsView: UIView!
	
	var todayLessons: [LessonViewModel] = [] {
		didSet {
			lessonsTableView.estimatedRowHeight = UITableView.automaticDimension
			lessonsTableView.reloadData()
		}
	}
	var nextLessons: [LessonViewModel] = [] {
		didSet {
			lessonsTableView.estimatedRowHeight = UITableView.automaticDimension
			lessonsTableView.reloadData()
		}
	}
	var transactions: [TransactionViewModel] = [] {
		didSet {
			transactionsTableView.estimatedRowHeight = UITableView.automaticDimension
			transactionsTableView.reloadData()
		}
	}
	var tasks: [TaskViewModel] = [] {
		didSet {
			homeworkCollectionView.reloadData()
		}
	}
	var attendances: [AttendanceViewModel] = [] {
		didSet {
			attendancesCollectionView.reloadData()
		}
	}
	var statistics: StatisticsViewModel?
	
	override func viewDidLayoutSubviews() {
		lessonTableViewHeight.constant = lessonsTableView.contentSize.height
		transactionsTableViewHeight.constant = transactionsTableView.contentSize.height
	}
	
	override func viewDidLoad() {
        super.viewDidLoad()
		navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "bell"), style: .plain, target: self, action: #selector(openNotifications))
		subscribe()
		setupViews()
		setupTableViews()
		setupCollections()
		loadData()
    }
	
	private func subscribe() {
		Notifications.attendanceChanged.subscribe(self, selector: #selector(loadExpiredPayments))
		Notifications.taskChanged.subscribe(self, selector: #selector(loadExpiredTasks))
	}
	
	deinit {
		NotificationCenter.default.removeObserver(self)
	}
	
	private func setupViews() {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "EEE, dd MMMM"
		dateFormatter.locale = Locale(identifier: "ru_RU")
		
		let monthFormatter = DateFormatter()
		monthFormatter.dateFormat = "LLLL"
		monthFormatter.locale = Locale(identifier: "ru_RU")
		todayLabel.text = dateFormatter.string(from: Date())
		statisticsMonthLabel.text = monthFormatter.string(from: Date()).capitalized
		
		calendarButton.layer.cornerRadius = 4
		calendarView.layer.cornerRadius = 4
		transactionsButton.layer.cornerRadius = 4
		transactionsView.layer.cornerRadius = 4
		statisticsView.layer.cornerRadius = 4
		statisticsButton.layer.cornerRadius = 4
		
		lessonsCardView.layer.cornerRadius = 8
		transactionsCardView.layer.cornerRadius = 8
		statisticsCardView.layer.cornerRadius = 8
		
		UIHelper.setViewShadow([lessonsCardView, transactionsCardView, statisticsCardView])
	}
	
	private func setupTableViews() {
		lessonsTableView.register(UINib(nibName: "LessonTableViewCell", bundle: nil), forCellReuseIdentifier: "LessonTableViewCell")
		lessonsTableView.dataSource = self
		lessonsTableView.delegate = self
		
		transactionsTableView.register(UINib(nibName: "TransactionTableViewCell", bundle: nil), forCellReuseIdentifier: "TransactionTableViewCell")
		transactionsTableView.dataSource = self
		transactionsTableView.delegate = self
		
		statisticsTableView.register(UINib(nibName: "StatisticsTableViewCell", bundle: nil), forCellReuseIdentifier: "StatisticsTableViewCell")
		statisticsTableView.dataSource = self
		statisticsTableView.delegate = self
	}
	
	private func setupCollections() {
		homeworkCollectionView.register(UINib(nibName: "ExpiredHomeworkCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ExpiredHomeworkCollectionViewCell")
		homeworkCollectionView.dataSource = self
		homeworkCollectionView.delegate = self
		
		attendancesCollectionView.register(UINib(nibName: "AttendanceCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AttendanceCollectionViewCell")
		attendancesCollectionView.dataSource = self
		attendancesCollectionView.delegate = self
	}
	
	private func loadData() {
		loadTodayLessons()
		loadNextLessons()
		loadTransactions()
		loadExpiredTasks()
		loadExpiredPayments()
		
		let calendar = Calendar.current
		let today = Date()
		let components = calendar.dateComponents(Set([.month, .year]), from: today)
		let startOfMonth = calendar.date(from: components)!
		var components2 = DateComponents()
		components2.month = 1
		components2.day = -1
		let endOfMonth = calendar.date(byAdding: components2, to: startOfMonth)!
		statistics = StatisticsViewModel(start: startOfMonth, end: endOfMonth)
	}
	
	private func loadTodayLessons() {
		LessonViewModel.loadTodayLessons { viewModels in
			self.todayLessons = viewModels
		}
	}
	
	private func loadNextLessons() {
		LessonViewModel.loadNextLessons { viewModels in
			self.nextLessons = viewModels
		}
	}
	
	private func loadTransactions() {
		TransactionViewModel.loadLastTransactions { viewModels in
			self.transactions = viewModels
		}
	}
	
	@objc func loadExpiredTasks() {
		TaskViewModel.loadData { viewModels in
			self.tasks = viewModels
		}
	}
	
	@objc func loadExpiredPayments() {
		AttendanceViewModel.getExpiredPayments { viewModels in
			self.attendances = viewModels
		}
	}
	
	@objc func openNotifications() {
		let controller = NotificationsListViewController.controller()
		navigationController?.pushViewController(controller, animated: true)
	}
	
	@IBAction func openCalendar(_ sender: Any) {
		tabBarController?.selectedIndex = 1
	}
	
	@IBAction func openTransactions(_ sender: Any) {
		let controller = AllTransactionsViewController.controller()
		navigationController?.pushViewController(controller, animated: true)
	}
	
	@IBAction func openStatistics(_ sender: Any) {
		tabBarController?.selectedIndex = 3
	}
}

extension DashboardViewController: UITableViewDataSource {
	func numberOfSections(in tableView: UITableView) -> Int {
		if tableView == lessonsTableView {
			return 2
		} else {
			return 1
		}
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if tableView == lessonsTableView {
			switch section {
			case 0:
				return todayLessons.count
			case 1:
				return nextLessons.count
			default:
				return 0
			}
		} else if tableView == transactionsTableView {
			return transactions.count > 0 ? transactions.count + 1 : 0
		} else {
			return 3
		}
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		if tableView == lessonsTableView {
			let cell = tableView.dequeueReusableCell(withIdentifier: "LessonTableViewCell", for: indexPath) as! LessonTableViewCell
			switch indexPath.section {
			case 0:
				cell.showTime = true
				cell.setup(with: todayLessons[indexPath.row])
			case 1:
				cell.showTime = false
				cell.setup(with: nextLessons[indexPath.row])
			default:
				break
			}
			return cell
		} else if tableView == transactionsTableView {
			let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTableViewCell", for: indexPath) as! TransactionTableViewCell
			if indexPath.row > 0 {
				cell.setupCell(with: transactions[indexPath.row - 1])
			}
			return cell
		} else {
			let cell = tableView.dequeueReusableCell(withIdentifier: "StatisticsTableViewCell", for: indexPath) as! StatisticsTableViewCell
			switch indexPath.row {
			case 0:
				cell.statisticsLabel.text = "Процент посещений"
				statistics?.attendancePercent.bind(listener: { value in
					cell.valueLabel.text = value
				})
			case 1:
				cell.statisticsLabel.text = "Всего заработано"
				statistics?.totalIncome.bind(listener: { value in
					cell.valueLabel.text = value
				})
			case 2:
				cell.statisticsLabel.text = "Количество занятий"
				statistics?.lessonsCount.bind(listener: { value in
					cell.valueLabel.text = value
				})
			default:
				break
			}
			return cell
		}
	}
	
	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		if tableView == lessonsTableView {
			switch section {
			case 0:
				return todayLessons.count == 0 ? nil : "Занятия на сегодня"
			case 1:
				return nextLessons.count == 0 ? nil : "Грядущие занятия"
			default:
				return nil
			}
		} else {
			return nil
		}
	}
}

extension DashboardViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		if tableView == transactionsTableView {
			if indexPath.row > 0 {
				let controller = TransactionViewController.controller()
				controller.transaction = transactions[indexPath.row - 1]
				let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: controller)
				bottomSheet.mdc_bottomSheetPresentationController?.preferredSheetHeight = 250
				present(bottomSheet, animated: true, completion: nil)
			}
		} else if tableView == lessonsTableView {
			var lesson: LessonViewModel?
			let controller = LessonViewController.controller()
			if indexPath.section == 0 {
				lesson = todayLessons[indexPath.row]
			} else {
				lesson = nextLessons[indexPath.row]
			}
			controller.lesson = lesson
			controller.contentMode = .simple
			navigationController?.pushViewController(controller, animated: true)
		}
	}
}

extension DashboardViewController: UICollectionViewDataSource {
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		if collectionView == homeworkCollectionView {
			return tasks.count
		} else {
			return attendances.count
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		if collectionView == homeworkCollectionView {
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExpiredHomeworkCollectionViewCell", for: indexPath) as! ExpiredHomeworkCollectionViewCell
			cell.viewModel = tasks[indexPath.row]
			return cell
		} else {
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttendanceCollectionViewCell", for: indexPath) as! AttendanceCollectionViewCell
			cell.viewModel = attendances[indexPath.row]
			return cell
		}
	}
}

extension DashboardViewController: UICollectionViewDelegate {
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		collectionView.deselectItem(at: indexPath, animated: true)
		if collectionView == attendancesCollectionView {
			let attendance = attendances[indexPath.row]
			let controller = AttendanceViewController.controller()
			controller.attendance = attendance
			let bottomSheet = MDCBottomSheetController(contentViewController: controller)
			bottomSheet.mdc_bottomSheetPresentationController?.preferredSheetHeight = 500
			present(bottomSheet, animated: true, completion: nil)
		} else if collectionView == homeworkCollectionView {
			let task = tasks[indexPath.row]
			let controller = TaskViewController.controller()
			controller.task = task
			let bottomSheet = MDCBottomSheetController(contentViewController: controller)
			bottomSheet.mdc_bottomSheetPresentationController?.preferredSheetHeight = 450
			present(bottomSheet, animated: true, completion: nil)
		}
	}
}
