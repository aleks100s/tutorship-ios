//
//  MenuViewController.swift
//  Tutorship
//
//  Created by Alexander on 22.05.2021.
//

import UIKit

class MenuViewController: BaseViewController {
	
	override class var storyboardName: String { "Main" }

	@IBOutlet weak var tableView: UITableView!
	
	var menuItems: [MenuItem] = [
		MenuItem(id: 1, title: "Профиль", iconName: "person.crop.circle"),
		MenuItem(id: 2, title: "Студенты", iconName: "person"),
		MenuItem(id: 3, title: "Группы", iconName: "person.3"),
		MenuItem(id: 4, title: "Настройки", iconName: "gear"),
		MenuItem(id: 5, title: "Обратная связь", iconName: "wave.3.backward.circle"),
		MenuItem(id: 6, title: "Поделиться приложением", iconName: "square.and.arrow.up"),
		MenuItem(id: 7, title: "Справка", iconName: "questionmark.circle"),
	]
	
	override func viewDidLoad() {
        super.viewDidLoad()
		setupTableView()
    }
	
	func setupTableView() {
		tableView.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuTableViewCell")
		tableView.dataSource = self
		tableView.delegate = self
	}
}

extension MenuViewController: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		menuItems.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as! MenuTableViewCell
		let menuItem = menuItems[indexPath.row]
		cell.menuTitle.text = menuItem.title
		cell.menuIcon.image = UIImage(systemName: menuItem.iconName)
		return cell
	}
}

extension MenuViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		var controller = UIViewController()
		switch indexPath.row {
		case 0:
			controller = ProfileViewController.controller()
		case 1:
			controller = StudentListViewController.controller()
		case 2:
			controller = GroupListViewController.controller()
		case 3:
			controller = SettingsViewController.controller()
		case 4:
			controller = FeedbackViewController.controller()
		case 5:
			controller = ShareAppViewController.controller()
		case 6:
			controller = InfoViewController.controller()
		default:
			break
		}
		navigationController?.pushViewController(controller, animated: true)
	}
}

class MenuItem {
	let id: Int
	let title: String
	let iconName: String
	
	init(id: Int, title: String, iconName: String) {
		self.id = id
		self.title = title
		self.iconName = iconName
	}
}
