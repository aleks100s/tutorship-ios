//
//  ProfileViewModel.swift
//  Tutorship
//
//  Created by Alexander on 24.05.2021.
//

import Foundation

class ProfileViewModel {
	init() {
		loadData()
	}
	
	func loadData() {
		Repository.shared.getProfileData { [unowned self] user in
			let fullName = "\(user.firstName) \(user.lastName)"
			userName.value = fullName
			email.value = user.email
		}
	}
	
	let userName: LiveData<String> = LiveData("")
	let email: LiveData<String> = LiveData("")
}
