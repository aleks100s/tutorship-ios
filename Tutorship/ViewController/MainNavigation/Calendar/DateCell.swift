//
//  DateCell.swift
//  Tutorship
//
//  Created by Alexander on 09.07.2021.
//

import JTAppleCalendar
import UIKit

class DateCell: JTAppleCell {
	
	@IBOutlet weak var circle: UIView!
	@IBOutlet weak var indicatorStack: UIStackView!
	@IBOutlet weak var dateLabel: UILabel!
	
	override func awakeFromNib() {
		circle.layer.cornerRadius = 13
		circle.layer.borderWidth = 1
		clearStack()
	}
	
	func clearStack() {
		indicatorStack.arrangedSubviews.forEach { view in
			view.removeFromSuperview()
		}
		indicatorStack.layoutIfNeeded()
	}
	
	func addIndicator(with color: UIColor) {
		let view = UIView()
		view.widthAnchor.constraint(equalToConstant: 6).isActive = true
		view.heightAnchor.constraint(equalToConstant: 6).isActive = true
		view.layer.cornerRadius = 3
		view.backgroundColor = color
		indicatorStack.addArrangedSubview(view)
	}
}
