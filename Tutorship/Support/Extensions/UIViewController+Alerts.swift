//
//  UIViewController+Alerts.swift
//  Tutorship
//
//  Created by Alexander on 24.05.2021.
//

import UIKit

extension UIViewController {
	func showAlert(_ message: String?) {
		showAlert(title: nil, message: message)
	}
	
	func showAlert(title: String?, message: String?) {
		showAlert(title: title, message: message, action: {})
	}
	
	func showAlert(title: String?, message: String?, action: @escaping () -> ()) {
		let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "Ок", style: .cancel, handler: { _ in
			action()
		}))
		presentAlert(alert)
	}
	
	func showDestructiveDialog(title: String?, buttonTitle: String?, action: @escaping () -> ()) {
		let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: {_ in}))
		alert.addAction(UIAlertAction(title: buttonTitle, style: .destructive, handler: { _ in
			action()
		}))
		presentAlert(alert)
	}
	
	func showDestructiveDialog(title: String?, message: String?, buttonTitle: String?, action: @escaping () -> ()) {
		let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: {_ in}))
		alert.addAction(UIAlertAction(title: buttonTitle, style: .destructive, handler: { _ in
			action()
		}))
		presentAlert(alert)
	}
	
	private func presentAlert(_ alert: UIAlertController) {
		alert.view.tintColor = Colors.secondary
		alert.setValue(NSAttributedString(string: alert.title ?? "", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: .medium), NSAttributedString.Key.foregroundColor : UIColor.white]), forKey: "attributedTitle")
		alert.setValue(NSAttributedString(string: alert.message ?? "", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.white]), forKey: "attributedMessage")

		present(alert, animated: true, completion: nil)
		let subview = (alert.view.subviews.first?.subviews.first?.subviews.first!)! as UIView
		subview.backgroundColor = Colors.primaryDark
	}
}
