//
//  StudentViewModel.swift
//  Tutorship
//
//  Created by Alexander on 25.05.2021.
//

import Foundation
import UIKit.UIColor

class StudentViewModel {
	static func loadData(completion: @escaping ([StudentViewModel]) -> Void) {
		Repository.shared.getStudentsData { students in
			let viewModels = students.map { student in
				StudentViewModel(student: student)
			}
			completion(viewModels)
		}
	}
	
	let initials: LiveData<String> = LiveData("")
	let color: LiveData<UIColor> = LiveData(Colors.white)
	let fullName: LiveData<String> = LiveData("")
	let phoneNumber: LiveData<String> = LiveData("")
	let school: LiveData<String> = LiveData("")
	let grade: LiveData<String> = LiveData("")
	let parentName: LiveData<String> = LiveData("")
	let parentPhone: LiveData<String> = LiveData("")
	let parentEmail: LiveData<String> = LiveData("")
	let id: LiveData<Int> = LiveData(0)
		
	init() {}
	
	init(student: Student) {
		setFields(student)
	}
	
	private func setFields(_ student: Student) {
		id.value = student.id
		let name = student.fullName
		let parts = name.split(separator: " ")
		if parts.count > 1 {
			initials.value = "\(parts[0].localizedCapitalized.first ?? " ")\(parts[1].localizedCapitalized.first ?? " ")"
		} else {
			if name.count > 1 {
				let index = name.index(name.startIndex, offsetBy: 1)
				initials.value = String(name[..<index])
			} else {
				initials.value = "\(name.first ?? " ")"
			}
		}
		color.value = UIColor(hex: student.color) ?? .white
		fullName.value = student.fullName
		phoneNumber.value = student.phone
		school.value = student.school
		grade.value = "\(student.grade) класс"
		parentName.value = student.parentName
		parentPhone.value = student.parentPhone
		parentEmail.value = student.parentEmail
	}
	
	func create(name: String, phone: String, school: String, grade: Int, parentName: String, parentPhone: String, parentEmail: String, completion: @escaping () -> Void) {
		let student = Student(fullName: name, school: school, grade: grade, phone: phone, parentName: parentName, parentPhone: parentPhone, parentEmail: parentEmail)
		Repository.shared.createStudent(student: student) { student in
			self.setFields(student)
			completion()
		}
	}
	
	func update(name: String, phone: String, school: String, grade: Int, parentName: String, parentPhone: String, parentEmail: String, completion: @escaping () -> Void) {
		let student = Student(id: id.value, fullName: name, school: school, grade: grade, phone: phone, parentName: parentName, parentPhone: parentPhone, parentEmail: parentEmail)
		Repository.shared.updateStudent(student: student) { student in
			self.setFields(student)
			completion()
		}
	}
	
	func delete() {
		Repository.shared.deleteStudent(studentID: self.id.value)
	}
}
