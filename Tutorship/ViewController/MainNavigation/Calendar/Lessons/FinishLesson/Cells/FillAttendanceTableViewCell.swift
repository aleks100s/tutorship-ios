//
//  FillAttendanceTableViewCell.swift
//  Tutorship
//
//  Created by Alexander on 20.07.2021.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextFields


class FillAttendanceTableViewCell: UITableViewCell {

	@IBOutlet weak var isPaidLabel: UILabel!
	@IBOutlet weak var studentNameLabel: UILabel!
	@IBOutlet weak var studentPhoneLabel: UILabel!
	@IBOutlet weak var attendanceSwitch: UISwitch!
	@IBOutlet weak var priceTextField: MDCOutlinedTextField!
	@IBOutlet weak var commentTextField: MDCOutlinedTextField!
	@IBOutlet weak var markTextField: MDCOutlinedTextField!
	
	var student: StudentViewModel? {
		didSet {
			student?.fullName.bind(listener: { name in
				self.studentNameLabel.text = name
			})
			student?.phoneNumber.bind(listener: { phone in
				self.studentPhoneLabel.text = phone
			})
		}
	}
	
	var attendance: AttendanceViewModel? {
		didSet {
			priceTextField.isEnabled = false
			commentTextField.isEnabled = false
			markTextField.isEnabled = false
			attendanceSwitch.isEnabled = false
			attendance?.studentName.bind(listener: { name in
				self.studentNameLabel.text = name
			})
			attendance?.studentPhone.bind(listener: { phone in
				self.studentPhoneLabel.text = phone
			})
			attendance?.studentAbsent.bind(listener: { isAbsent in
				self.attendanceSwitch.isOn = !isAbsent
			})
			attendance?.lessonPrice.bind(listener: { price in
				self.priceTextField.text = price
			})
			attendance?.lessonComment.bind(listener: { comment in
				self.commentTextField.text = comment
			})
			attendance?.studentMark.bind(listener: { mark in
				self.markTextField.text = mark == 0 ? "" : "\(mark)"
			})
			attendance?.attendancePaid.bind(listener: { isPaid in
				self.isPaidLabel.textColor = isPaid ? .systemGreen : .systemRed
				self.isPaidLabel.text = isPaid ? "Оплачено:" : "К оплате:"
			})
		}
	}
	
	override func awakeFromNib() {
        super.awakeFromNib()
		UIHelper.prepareTextFields(textFields: [commentTextField, markTextField, priceTextField])
		commentTextField.label.text = "Комментарий"
		markTextField.label.text = "Оценка"
		priceTextField.label.text = "Стоимость"

    }
	
	@IBAction func attendanceSwitched(_ sender: UISwitch) {
		commentTextField.isEnabled = sender.isOn
		markTextField.isEnabled = sender.isOn
		priceTextField.isEnabled = sender.isOn
	}
}
