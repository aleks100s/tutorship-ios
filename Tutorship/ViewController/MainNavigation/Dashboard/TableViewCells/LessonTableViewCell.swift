//
//  LessonTableViewCell.swift
//  Tutorship
//
//  Created by Alexander on 03.06.2021.
//

import UIKit

class LessonTableViewCell: UITableViewCell {

	@IBOutlet weak var circleView: UIView!
	@IBOutlet weak var groupLabel: UILabel!
	@IBOutlet weak var topicLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!
	@IBOutlet weak var timeFromLabel: UILabel!
	@IBOutlet weak var timeToLabel: UILabel!
	
	var showTime: Bool = false
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		circleView.layer.cornerRadius = 8
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
		
	}
	
	func setup(with viewModel: LessonViewModel) {
		viewModel.groupColor.bind { color in
			self.circleView.backgroundColor = color
		}
		viewModel.groupName.bind { name in
			self.groupLabel.text = name
		}
		viewModel.lessonTopic.bind { topic in
			self.topicLabel.text = topic
		}
		viewModel.lessonDate.bind { date in
			self.dateLabel.text = date
		}
		viewModel.lessonTimeStart.bind { time in
			self.timeFromLabel.text = time
		}
		viewModel.lessonTimeEnd.bind { time in
			self.timeToLabel.text = time
		}
		dateLabel.isHidden = showTime
		timeFromLabel.isHidden = !showTime
		timeToLabel.isHidden = !showTime
	}
    
}
