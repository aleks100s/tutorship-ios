//
//  Progress.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Foundation

class Progress: Codable, BaseModel {
	var id: Int = 0
	var groupID: Int = 0
	var studentID: Int = 0
	var discount: Int = 0
	var comment: String = ""
	var balance: Double = 0
	
	required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		id  = try container.decode(Int.self, forKey: .id)
		groupID = try container.decode(Int.self, forKey: .groupID)
		studentID = try container.decode(Int.self, forKey: .studentID)
		discount = try container.decode(Int.self, forKey: .discount)
		comment = try container.decode(String.self, forKey: .comment)
		balance = try container.decode(Double.self, forKey: .balance)
	}
	
	enum CodingKeys: String, CodingKey {
		case id
		case groupID = "group"
		case studentID = "student"
		case discount
		case comment
		case balance
	}
}
