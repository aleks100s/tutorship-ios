//
//  GroupListViewController.swift
//  Tutorship
//
//  Created by Alexander on 23.05.2021.
//

import UIKit

class GroupListViewController: BaseViewController {
	override class var storyboardName: String { "Groups" }

	@IBOutlet weak var tableView: UITableView!
	
	var viewModels: [GroupViewModel] = [] {
		didSet {
			tableView.reloadData()
		}
	}
	
	override func viewDidLoad() {
        super.viewDidLoad()
		navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addGroupPressed))
		setupTableView()
    }
	
	func setupTableView() {
		tableView.register(UINib(nibName: "GroupTableViewCell", bundle: nil), forCellReuseIdentifier: "GroupTableViewCell")
		tableView.dataSource = self
		tableView.delegate = self
	}
	
	override func viewWillAppear(_ animated: Bool) {
		loadData()
	}
	
	func loadData() {
		GroupViewModel.loadData { groupViewModels in
			self.viewModels = groupViewModels
		}
	}
	
	@objc func addGroupPressed() {
		let viewController = GroupDetailViewController.controller()
		viewController.contentMode = .creating
		navigationController?.pushViewController(viewController, animated: true)
	}
}

extension GroupListViewController: UITableViewDataSource, UITableViewDelegate {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		viewModels.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "GroupTableViewCell", for: indexPath) as! GroupTableViewCell
		cell.viewModel = viewModels[indexPath.row]
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		let viewModel = viewModels[indexPath.row]
		let viewController = GroupDetailViewController.controller()
		viewController.contentMode = .simple
		viewController.viewModel = viewModel
		navigationController?.pushViewController(viewController, animated: true)
	}
	
	func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
			-> UISwipeActionsConfiguration? {
		let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in
			tableView.beginUpdates()
			let viewModel = self.viewModels.remove(at: indexPath.row)
			tableView.deleteRows(at: [indexPath], with: .fade)
			viewModel.delete()
			tableView.endUpdates()
			completionHandler(true)
		}
		deleteAction.image = UIImage(systemName: "trash")
		deleteAction.backgroundColor = .systemRed
		let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
		return configuration
	}
}
