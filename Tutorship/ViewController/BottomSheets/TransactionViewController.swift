//
//  TransactionViewController.swift
//  Tutorship
//
//  Created by Alexander on 14.06.2021.
//

import UIKit

class TransactionViewController: BaseViewController {
	override class func controller() -> Self {
		return TransactionViewController(nibName: String(describing: Self.self), bundle: nil) as! Self
	}
	@IBOutlet weak var studentNameLabel: UILabel!
	@IBOutlet weak var lessonDateLabel: UILabel!
	@IBOutlet weak var paymentDateLabel: UILabel!
	@IBOutlet weak var amountLabel: UILabel!
	
	var transaction: TransactionViewModel?
	
	override func viewDidLoad() {
        super.viewDidLoad()
		setupViews()
    }

	private func setupViews() {
		transaction?.transactionStudent.bind { value in
			self.studentNameLabel.text = value
		}
		transaction?.transactionTimestamp.bind { value in
			self.paymentDateLabel.text = value
		}
		transaction?.transactionAmount.bind { value in
			self.amountLabel.text = value
		}
	}
}
