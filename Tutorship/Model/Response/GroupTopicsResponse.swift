//
//  GroupTopicsResponse.swift
//  Tutorship
//
//  Created by Alexander on 18.05.2021.
//

import Foundation

class GroupTopicsResponse: Decodable, BaseModel {
	var topics: [DateTopic]
}
