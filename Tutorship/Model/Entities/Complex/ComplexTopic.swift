//
//  ComplexTopic.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Foundation

class ComplexTopic: Codable, BaseModel {
	var lessonID: Int = 0
	var date: Date = Date()
	var title: String = ""
	
	required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		lessonID = try container.decode(Int.self, forKey: .lessonID)
		let date = try container.decode(Double.self, forKey: .date)
		self.date = Date(timeIntervalSince1970: date / 1000)
		title = try container.decode(String.self, forKey: .title)
	}
}
