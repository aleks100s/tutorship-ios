//
//  DebtResponse.swift
//  Tutorship
//
//  Created by Alexander on 18.05.2021.
//

import Foundation

class DebtResponse: Decodable, BaseModel {
	var debt: Float
}
