//
//  LessonTriggerScheduleTableViewCell.swift
//  Tutorship
//
//  Created by Alexander on 11.07.2021.
//

import UIKit

protocol LessonTriggerScheduleTableViewCellDelegate: AnyObject {
	func scheduleTriggered(_ isOn: Bool)
}

class LessonTriggerScheduleTableViewCell: UITableViewCell {

	weak var delegate: LessonTriggerScheduleTableViewCellDelegate?
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

	@IBAction func triggered(_ sender: UISwitch) {
		delegate?.scheduleTriggered(sender.isOn)
	}
}
