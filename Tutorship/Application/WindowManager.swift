//
//  WindowManager.swift
//  Tutorship
//
//  Created by Alexander on 22.05.2021.
//

import UIKit

class WindowManager {
	static let shared = WindowManager()
	
	var window: UIWindow?
	
	private init() {
		NotificationCenter.default.addObserver(self, selector: #selector(previewWatched(notification:)), name: Notifications.previewWatched.name, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(userAuthorized(notification:)), name: Notifications.userAuthorized.name, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(logout), name: Notifications.signOut.name, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(showAlert(notification:)), name: Notifications.errorOccured.name, object: nil)
	}
	
	deinit {
		NotificationCenter.default.removeObserver(self)
	}
	
	@objc func previewWatched(notification: Notification) {
		if let window = self.window {
			Defaults.previewWatched = true
			openAuthenticationController(window: window)
		}
	}
	
	@objc func userAuthorized(notification: Notification) {
		if let response = notification.object as? AuthResponse, let window = self.window {
			Defaults.token = response.token
			Defaults.userID = response.userID
			openMainController(window: window)
		}
	}
	
	@objc func logout() {
		Defaults.deleteAllKey()
		if let window = self.window {
			openAuthenticationController(window: window)
		}
	}
	
	func playApplication(_ window: UIWindow) {
		self.window = window
		if Defaults.previewWatched {
			if Defaults.token != nil {
				openMainController(window: window)
			} else {
				openAuthenticationController(window: window)
			}
		} else {
			openPreview(window: window)
		}
	}
	
	func openMainController(window: UIWindow) {
		let vc = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
		window.rootViewController = vc
	}
	
	func openAuthenticationController(window: UIWindow) {
		let vc = UIStoryboard(name: "Authentication", bundle: nil).instantiateInitialViewController()
		window.rootViewController = vc
	}
	
	func openPreview(window: UIWindow) {
		let vc = UIStoryboard(name: "Preview", bundle: nil).instantiateInitialViewController()
		window.rootViewController = vc
	}
	
	@objc func showAlert(notification: Notification) {
		if let text = notification.object as? String, var topController = window?.rootViewController {
//			while let presentedViewController = topController.presentedViewController {
//				if !(presentedViewController is UIAlertController) {
//					topController = presentedViewController
//				}
//			}
			topController.showAlert(text)
		}
	}
}
