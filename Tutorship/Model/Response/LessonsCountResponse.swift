//
//  LessonsCountResponse.swift
//  Tutorship
//
//  Created by Alexander on 18.05.2021.
//

import Foundation

class LessonsCountResponse: Decodable, BaseModel {
	var lessonsCount: Int
	
	enum CodingKeys: String, CodingKey {
		case lessonsCount = "lessons_count"
	}
}
