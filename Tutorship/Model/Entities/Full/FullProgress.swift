//
//  FullProgress.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Foundation

class FullProgress: Codable, BaseModel {
	var progress: Progress?
	var student: Student?
	var group: Group?
	
	required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		progress = try container.decode(Progress.self, forKey: .progress)
		student = try container.decode(Student.self, forKey: .student)
		group = try container.decode(Group.self, forKey: .group)
	}
	
	enum CodingKeys: String, CodingKey {
		case progress = "progress_data"
		case student = "student_data"
		case group = "group_data"
	}
}
