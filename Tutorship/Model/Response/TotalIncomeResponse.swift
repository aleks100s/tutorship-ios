//
//  TotalIncomeResponse.swift
//  Tutorship
//
//  Created by Alexander on 18.05.2021.
//

import Foundation

class TotalIncomeResponse: Decodable, BaseModel {
	var totalIncome: Double
	
	enum CodingKeys: String, CodingKey {
		case totalIncome = "total_income"
	}
}
