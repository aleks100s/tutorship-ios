//
//  AttendanceEntity.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Foundation

class Attendance: Codable, BaseModel {
	required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		id  = try container.decode(Int.self, forKey: .id)
		studentID = try container.decode(Int.self, forKey: .studentID)
		lessonID = try container.decode(Int.self, forKey: .lessonID)
		isAbsent = try container.decode(Bool.self, forKey: .isAbsent)
		mark = try container.decode(Int.self, forKey: .mark)
		price = try container.decode(Int.self, forKey: .price)
		isPaid = try container.decode(Bool.self, forKey: .isPaid)
		comment = try container.decode(String.self, forKey: .comment)
	}
	
	init(id: Int, studentID: Int, lessonID: Int, isAbsent: Bool, mark: Int, price: Int, isPaid: Bool, comment: String) {
		self.id = id
		self.studentID = studentID
		self.lessonID = lessonID
		self.isAbsent = isAbsent
		self.mark = mark
		self.price = price
		self.isPaid = isPaid
		self.comment = comment
	}
	
	var id: Int = 0
	var studentID: Int = 0
	var lessonID: Int = 0
	var isAbsent: Bool = false
	var mark: Int = 0
	var price: Int = 0
	var isPaid: Bool = false
	var comment: String = ""
	
	enum CodingKeys: String, CodingKey {
		case id
		case studentID = "student"
		case lessonID = "lesson"
		case isAbsent
		case mark
		case price
		case isPaid
		case comment
	}
}
