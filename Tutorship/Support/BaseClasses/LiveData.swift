//
//  Box.swift
//  Tutorship
//
//  Created by Alexander on 25.05.2021.
//

import Foundation

class LiveData<T> {
	typealias Listener = (_ value: T) -> ()
	
	private var listener: Listener?
	
	var value: T {
		didSet {
		  listener?(value)
		}
	}
	
	init(_ value: T) {
		self.value = value
	}
	
	func bind(listener: Listener?) {
		self.listener = listener
		listener?(value)
	}
}
