//
//  Extensions.swift
//  Tutorship
//
//  Created by Alexander on 22.05.2021.
//

import UIKit

extension UIColor {
	public convenience init?(hex: String) {
		let r, g, b, a: CGFloat
		
		var hexColor = hex

		if hex.hasPrefix("#") {
			let start = hex.index(hex.startIndex, offsetBy: 1)
			hexColor = String(hex[start...])
		}

		if hexColor.count == 8 {
			let scanner = Scanner(string: hexColor)
			var hexNumber: UInt64 = 0

			if scanner.scanHexInt64(&hexNumber) {
				a = CGFloat((hexNumber & 0xff000000) >> 24) / 255
				r = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
				g = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
				b = CGFloat(hexNumber & 0x000000ff) / 255

				self.init(red: r, green: g, blue: b, alpha: a)
				return
			}
		} else if (hexColor.count == 6) {
			let scanner = Scanner(string: hexColor)
			var hexNumber: UInt64 = 0

			if scanner.scanHexInt64(&hexNumber) {
				r = CGFloat((hexNumber & 0xff0000) >> 16) / 255
				g = CGFloat((hexNumber & 0x00ff00) >> 8) / 255
				b = CGFloat(hexNumber & 0x0000ff) / 255
				a = 1

				self.init(red: r, green: g, blue: b, alpha: a)
				return
			}
		}

		return nil
	}
}

extension UIColor {
	func hexString() -> String {
		let components = self.cgColor.components
		let r: CGFloat = components?[0] ?? 0.0
		let g: CGFloat = components?[1] ?? 0.0
		let b: CGFloat = components?[2] ?? 0.0

		let hexString = String.init(format: "%02lX%02lX%02lX", lroundf(Float(r * 255)), lroundf(Float(g * 255)), lroundf(Float(b * 255)))
		print(hexString)
		return hexString
	 }
}
