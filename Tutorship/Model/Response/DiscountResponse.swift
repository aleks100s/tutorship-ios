//
//  DiscountResponse.swift
//  Tutorship
//
//  Created by Alexander on 18.05.2021.
//

import Foundation

class DiscountResponse: Decodable, BaseModel {
	var discount: Int
}
