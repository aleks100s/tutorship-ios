//
//  BaseViewController.swift
//  Tutorship
//
//  Created by Alexander on 22.05.2021.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class BaseViewController: UIViewController {
	
	class func controller() -> Self {
		return UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(withIdentifier: String(describing: Self.self)) as! Self
	}
	
	class var storyboardName: String {
		return ""
	}
	
	var contentMode: ContentMode = .simple
	
	enum ContentMode {
		case creating
		case simple
		case editing
	}
}

extension BaseViewController: UITextFieldDelegate {
	func textFieldDidBeginEditing(_ textField: UITextField) {
		if let field = textField as? MDCOutlinedTextField {
			UIHelper.removeError(from: field)
		}
	}
}
