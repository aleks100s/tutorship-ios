//
//  FullAttendance.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Foundation

class FullAttendance: Codable, BaseModel {
	var attendance: Attendance?
	var student: Student?
	var lesson: Lesson?
	
	required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		attendance = try container.decode(Attendance.self, forKey: .attendance)
		student = try container.decode(Student.self, forKey: .student)
		lesson = try container.decode(Lesson.self, forKey: .lesson)
	}
	
	enum CodingKeys: String, CodingKey {
		case attendance = "attendance_data"
		case student = "student_data"
		case lesson = "lesson_data"
	}
}
