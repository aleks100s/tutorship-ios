//
//  MyError.swift
//  Tutorship
//
//  Created by Alexander on 16.05.2021.
//

import Foundation

public enum MyError: Error {
	case noInternet
	case unauthorized
	case unknownError
	case errorWithText(String)
}

extension MyError: CustomStringConvertible {
	/// Описание ошибки на русском языке
	public var description: String {
		switch self {
		case .noInternet: return "Нет интернета"
		case .unauthorized: return "Не авторизован"
		case .unknownError: return "Неизвестная ошибка"
		case .errorWithText(let text): return text
		}
	}
}
