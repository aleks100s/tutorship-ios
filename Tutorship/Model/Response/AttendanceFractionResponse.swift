//
//  AttendanceFractionResponse.swift
//  Tutorship
//
//  Created by Alexander on 18.05.2021.
//

import Foundation

class AttendanceFractionResponse: Codable, BaseModel {
	var attendanceFraction: String
	
	enum CodingKeys: String, CodingKey {
		case attendanceFraction = "attendance_fraction"
	}
}
