//
//  ProfileViewController.swift
//  Tutorship
//
//  Created by Alexander on 24.05.2021.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextFields
class ProfileViewController: BaseViewController {
	
	override class var storyboardName: String {
		return "Profile"
	}

	@IBOutlet weak var fullNameTextField: MDCOutlinedTextField!
	@IBOutlet weak var emailTextField: MDCOutlinedTextField!
	
	var profileViewModel = ProfileViewModel()
	
	override func viewDidLoad() {
        super.viewDidLoad()
		setupViews()
    }
	
	func setupViews() {
		self.title = "Профиль"
		UIHelper.prepareTextFields(textFields: [fullNameTextField, emailTextField])
		
		fullNameTextField.label.text = "Имя пользователя"
		fullNameTextField.placeholder = "Имя пользователя"
		emailTextField.label.text = "E-Mail"
		
		profileViewModel.userName.bind { value in
			self.fullNameTextField.text = value
		}
		profileViewModel.email.bind { value in
			self.emailTextField.text = value
		}
	}

	@IBAction func signOutPressed(_ sender: Any) {
		showDestructiveDialog(title: "Вы уверены, что хотите выйти?", buttonTitle: "Выйти") {
			Notifications.signOut.post()
		}
	}
}
