//
//  CalendarViewController.swift
//  Tutorship
//
//  Created by Alexander on 22.05.2021.
//

import UIKit
import JTAppleCalendar

struct Month: Hashable {
	var startDate: Date
	var endDate: Date
	
	func hash(into hasher: inout Hasher) {
		hasher.combine(startDate.timeIntervalSince1970)
		hasher.combine(endDate.timeIntervalSince1970)
	}
}

class CalendarViewController: BaseViewController {
	
	override class var storyboardName: String { "Main" }
	
	@IBOutlet weak var monthLabel: UILabel!
	@IBOutlet weak var calendarView: JTAppleCalendarView!
	@IBOutlet weak var yearLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!
	@IBOutlet weak var tableView: UITableView!
	
	var lessons = Dictionary<Double, [LessonViewModel]>()
	
	var selectedDateLessons: [LessonViewModel] = [] {
		didSet {
			tableView.reloadData()
		}
	}
	
	var selectedDate = Date() {
		didSet {
			dateLabel.text = dateFormatter.string(from: selectedDate)
			selectedDateLessons = lessons[selectedDate.timeIntervalSince1970] ?? []
		}
	}
	
	var calendar: Calendar = {
		let calendar = Calendar.current
		return calendar
	}()
	
	var yearFormatter: DateFormatter = {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "yyyy"
		return dateFormatter
	}()
	
	var monthFormatter: DateFormatter = {
		let dateFormatter = DateFormatter()
		dateFormatter.locale = Locale(identifier: "ru_RU")
		dateFormatter.dateFormat = "LLLL"
		return dateFormatter
	}()
	
	var dateFormatter: DateFormatter = {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "dd.MM.yyyy"
		return dateFormatter
	}()
	
	override func viewDidLoad() {
        super.viewDidLoad()
		setupCalendar()
		setupTableView()
    }
	
	@IBAction func addNewLessonPressed(_ sender: Any) {
		let controller = LessonViewController.controller()
		controller.contentMode = .creating
		controller.pickedDate = calendarView.selectedDates.first ?? Date()
		navigationController?.pushViewController(controller, animated: true)
	}
	
	private func setupCalendar() {
		calendarView.calendarDataSource = self
		calendarView.calendarDelegate = self
		calendarView.scrollToDate(Date())
		calendarView.allowsMultipleSelection = false
		calendarView.allowsSelection = true
		updateLabels(with: Date())
	}
	
	private func setupTableView() {
		tableView.register(UINib(nibName: "CalendarLessonTableViewCell", bundle: nil), forCellReuseIdentifier: "CalendarLessonTableViewCell")
		tableView.dataSource = self
		tableView.delegate = self
	}
	
	private func updateLabels(with date: Date) {
		yearLabel.text = yearFormatter.string(from: date)
		monthLabel.text = monthFormatter.string(from: date).capitalized
	}
	
	private func loadLessons(from startDate: Date, to endDate: Date) {
		LessonViewModel.loadLessons(from: startDate, to: endDate) { [weak self] lessons in
			for lesson in lessons {
				let date = lesson.dateMillis
				if self?.lessons[date] == nil {
					var list = [LessonViewModel]()
					list.append(lesson)
					self?.lessons[date] = list
				} else {
					if !(self?.lessons[date]?.contains(where: { l in
						l.lessonID.value == lesson.lessonID.value
					}) ?? false) {
						self?.lessons[date]?.append(lesson)
					}
				}
			}
			self?.calendarView.reloadData()
		}
	}
}

extension CalendarViewController: JTAppleCalendarViewDataSource {
	func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
		let startDate = Date().addingTimeInterval(60 * 60 * 24 * -365)
		let endDate = Date().addingTimeInterval(60 * 60 * 24 * 365)
		return ConfigurationParameters(startDate: startDate, endDate: endDate, numberOfRows: 6, firstDayOfWeek: .monday)
	}
	
	func configureCell(view: JTAppleCell?, cellState: CellState) {
		guard let cell = view as? DateCell  else { return }
		cell.dateLabel.text = cellState.text
		
		handleCellBorderColor(cell: cell, cellState: cellState)
		handleCellCircleColor(cell: cell, cellState: cellState)
		handleCellTextColor(cell: cell, cellState: cellState)
		handleCellIndicators(cell: cell, cellState: cellState)
	}
		
	func handleCellTextColor(cell: DateCell, cellState: CellState) {
		if cellState.dateBelongsTo == .thisMonth {
			cell.dateLabel.textColor = .white
		} else {
			cell.dateLabel.textColor = .gray
		}
	}
	
	func handleCellBorderColor(cell: DateCell, cellState: CellState) {
		if calendar.isDateInToday(cellState.date) && cellState.dateBelongsTo == .thisMonth{
			cell.circle.layer.borderColor = Colors.accent.cgColor
		} else {
			cell.circle.layer.borderColor = UIColor.clear.cgColor
		}
	}
	
	func handleCellCircleColor(cell: DateCell, cellState: CellState) {
		if selectedDate == cellState.date && cellState.dateBelongsTo == .thisMonth {
			cell.circle.layer.backgroundColor = Colors.accentWeak.cgColor
		} else {
			cell.circle.layer.backgroundColor = UIColor.clear.cgColor
		}
	}
	
	func handleCellIndicators(cell: DateCell, cellState: CellState) {
		cell.clearStack()
		if let lessons = self.lessons[cellState.date.timeIntervalSince1970] {
			lessons.forEach { lesson in
				cell.addIndicator(with: lesson.groupColor.value)
			}
		}
	}
}

extension CalendarViewController: JTAppleCalendarViewDelegate {
	func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
	   let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "dateCell", for: indexPath) as! DateCell
	   self.calendar(calendar, willDisplay: cell, forItemAt: date, cellState: cellState, indexPath: indexPath)
	   return cell
	}
		
	func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
	   configureCell(view: cell, cellState: cellState)
	}
	
	func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
		let startDate = self.calendar.startOfDay(for: visibleDates.monthDates.first?.date ?? Date())
		let endDate = self.calendar.startOfDay(for: visibleDates.monthDates.last?.date ?? Date())
		updateLabels(with: startDate)
		loadLessons(from: startDate, to: endDate)
	}
	
	func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
		selectedDate = Date()
		configureCell(view: cell, cellState: cellState)
	}
	
	func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
		selectedDate = date
		configureCell(view: cell, cellState: cellState)
	}
}

extension CalendarViewController: UITableViewDataSource, UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		selectedDateLessons.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "CalendarLessonTableViewCell", for: indexPath) as! CalendarLessonTableViewCell
		cell.lesson = selectedDateLessons[indexPath.row]
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		let lesson = selectedDateLessons[indexPath.row]
		if lesson.lessonStartDate.value < Date() {
			let controller = FinishLessonViewController.controller()
			controller.lesson = lesson
			self.navigationController?.pushViewController(controller, animated: true)
		} else {
			let controller = LessonViewController.controller()
			controller.contentMode = .simple
			controller.lesson = lesson
			self.navigationController?.pushViewController(controller, animated: true)
		}
	}
}
