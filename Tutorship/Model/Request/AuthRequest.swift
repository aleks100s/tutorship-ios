//
//  AuthRequest.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Foundation

class AuthRequest: Encodable, BaseModel {
	var username: String
	var password: String
	var email: String
	var name: String
	
	init(password: String, email: String, name: String) {
		self.username = email
		self.password = password
		self.email = email
		self.name = name
	}
	
	init(email: String, password: String) {
		self.email = email
		self.password = password
		self.username = email
		self.name = ""
	}
}
