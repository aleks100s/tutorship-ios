//
//  UserResponse.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Foundation

class UserResponse: Decodable, BaseModel {
	var firstName: String
	var lastName: String
	var email: String
	var username: String
	
	enum CodingKeys: String, CodingKey {
		case firstName = "first_name"
		case lastName = "last_name"
		case email
		case username
	}
}
