//
//  LessonTopTableViewCell.swift
//  Tutorship
//
//  Created by Alexander on 09.07.2021.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextFields

protocol LessonTopTableViewCellDelegate: AnyObject {
	func addStudentOrGroup()
	func selectGroup()
	func topicPicked(_ topic: String)
	func topicRemoved(_ topic: String)
	func priceSet(_ price: Int)
}

class LessonTopTableViewCell: UITableViewCell, UITextFieldDelegate {
	
	func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
		delegate?.priceSet(Int(textField.text ?? "") ?? 0)
	}
	
	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var pickGroupButton: UIButton!
	@IBOutlet weak var studentGroupDropdown: MDCOutlinedTextField!
	@IBOutlet weak var topicsDropdown: MDCOutlinedTextField!
	@IBOutlet weak var lessonPriceTextField: MDCOutlinedTextField!
	
	weak var delegate: LessonTopTableViewCellDelegate?
	var isEnabled: Bool = true {
		didSet {
			studentGroupDropdown.isEnabled = isEnabled
			topicsDropdown.isEnabled = isEnabled
			lessonPriceTextField.isEnabled = isEnabled
			pickGroupButton.isEnabled = isEnabled
		}
	}
	var group: Group? {
		didSet {
			studentGroupDropdown.text = group?.name
		}
	}
	var topic: String? {
		didSet {
			topicsDropdown.text = topic
		}
	}
	
	var pickedTopics: [String] = [] {
		didSet {
			collectionView.reloadData()
		}
	}
	
    override func awakeFromNib() {
        super.awakeFromNib()
		lessonPriceTextField.delegate = self
		lessonPriceTextField.overrideUserInterfaceStyle = .light
		setupCollectionView()
		pickGroupButton.imageView?.tintColor = Colors.accent
		UIHelper.prepareTextFields(textFields: [studentGroupDropdown, topicsDropdown, lessonPriceTextField])
    }
	
	private func setupCollectionView() {
		collectionView.register(UINib(nibName: "TopicCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TopicCollectionViewCell")
		collectionView.dataSource = self
	}
	
	func setup(group: String, topics: [String], price: String) {
		lessonPriceTextField.text = price
		pickedTopics = topics
		topicsDropdown.text = topics.last
		studentGroupDropdown.text = group
	}
    
	@IBAction func pickGroupPressed(_ sender: Any) {
		delegate?.selectGroup()
	}
	
	@IBAction func addStudentOrGroupPressed(_ sender: Any) {
		delegate?.addStudentOrGroup()
	}
	
	@IBAction func addTopicPressed(_ sender: Any) {
		let topic = topicsDropdown.text ?? "¿error?"
		topicPicked(topic)
	}
	
	private func topicPicked(_ topic: String) {
		topicsDropdown.text = ""
		delegate?.topicPicked(topic)
	}
}

extension LessonTopTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate {
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		pickedTopics.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopicCollectionViewCell", for: indexPath) as! TopicCollectionViewCell
		cell.topic = pickedTopics[indexPath.row]
		cell.delegate = self
		return cell
	}
}

extension LessonTopTableViewCell: TopicCollectionViewCellDelegate {
	func topicRemoved(_ topic: String?) {
		if isEnabled {
			pickedTopics.removeAll { t in
				t == topic
			}
			delegate?.topicRemoved(topic ?? "")
		}
	}
}
