//
//  LessonViewController.swift
//  Tutorship
//
//  Created by Alexander on 09.07.2021.
//

import UIKit
import MaterialComponents.MaterialBottomSheet

class LessonViewController: BaseViewController {
	
	override class func controller() -> Self {
		return LessonViewController(nibName: String(describing: Self.self), bundle: nil) as! Self
	}
	
	@IBOutlet weak var tableView: UITableView!
	
	var lesson: LessonViewModel?
	var topics: [String] = [] {
		didSet {
			tableView.reloadSections([0], with: .automatic)
		}
	}
	var group: Group? {
		didSet {
			tableView.reloadSections([0], with: .automatic)
		}
	}
	
	var lessonPrice: Int = 0
	
	var pickedDate: Date = Date() {
		didSet {
			if isViewLoaded {
				if tableView.numberOfSections > 3 {
					tableView.reloadSections([3], with: .automatic)
				}
			}

		}
	}
	var pickedTime: Date = Date()
	var pickedDuration: Int = 0
	
	private var scheduleActive: Bool = false {
		didSet {
			tableView.reloadData()
		}
	}
	var scheduleType: ScheduleType = .workDays
	var endDate: Date?
	var selectedDays: [Weekday] = [] {
		didSet {
			if tableView.numberOfSections > 3 {
				tableView.reloadSections([3], with: .automatic)
			}
		}
	}

    override func viewDidLoad() {
        super.viewDidLoad()
		setupTableView()
		if lesson != nil {
			showLesson()
			title = "Занятие \(lesson?.lessonDate.value ?? "?")"
			navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deleteLesson))
		} else {
			title = "Новое занятие"
			navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Создать", style: .done, target: self, action: #selector(saveLesson))
		}
    }
	
	private func setupTableView() {
		tableView.register(UINib(nibName: "LessonScheduleTableViewCell", bundle: nil), forCellReuseIdentifier: "LessonScheduleTableViewCell")
		tableView.register(UINib(nibName: "LessonTimeTableViewCell", bundle: nil), forCellReuseIdentifier: "LessonTimeTableViewCell")
		tableView.register(UINib(nibName: "LessonTopTableViewCell", bundle: nil), forCellReuseIdentifier: "LessonTopTableViewCell")
		tableView.register(UINib(nibName: "LessonTriggerScheduleTableViewCell", bundle: nil), forCellReuseIdentifier: "LessonTriggerScheduleTableViewCell")
		tableView.dataSource = self
		tableView.delegate = self
	}
	
	private func showLesson() {
		tableView.reloadData()
	}
	
	private func getTimeComponents(from date: Date) -> DateComponents {
		return Calendar.current.dateComponents([.hour, .minute, .second], from: date)
	}
	
	@objc func saveLesson() {
		let groupID = group?.id ?? 0
		let topics = topics.joined(separator: ";")
		let price = lessonPrice
		let components = getTimeComponents(from: pickedTime)
		let datetime = Calendar.current.date(bySettingHour: components.hour ?? 1, minute: components.minute ?? 0, second: 0, of: pickedDate) ?? Date()
		print(datetime.timeIntervalSince1970 * 1000)
		let duration = Double(pickedDuration)

		if scheduleActive {
			let endDate = Calendar.current.startOfDay(for: endDate!)
			LessonViewModel.createMultipleLessons(groupID: groupID, topics: topics, price: price, startDate: datetime, duration: duration, endDate: endDate, scheduleType: scheduleType, weekdays: selectedDays) {
				self.finishCreation()
			}
		} else {
			LessonViewModel.create(datetime: datetime, duration: duration, price: price, groupID: groupID, topic: topics) {
				self.finishCreation()
			}
		}
	}
	
	private func finishCreation() {
		showAlert(title: "Урок успешно создан", message: nil) {
			self.navigationController?.popViewController(animated: true)
		}
	}
	
	@objc func deleteLesson() {
		lesson?.deleteLesson {
			self.showAlert(title: "Урок успешно удален", message: nil) {
				self.navigationController?.popViewController(animated: true)
			}
		}
	}
}

extension LessonViewController: UITableViewDataSource, UITableViewDelegate {
	func numberOfSections(in tableView: UITableView) -> Int {
		scheduleActive ? 4 : 3
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		1
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		switch indexPath.section {
		case 3:
			let cell = tableView.dequeueReusableCell(withIdentifier: "LessonScheduleTableViewCell", for: indexPath) as! LessonScheduleTableViewCell
			cell.delegate = self
			cell.startDate = pickedDate
			cell.weekdays = selectedDays
			return cell
		case 2:
			let cell = tableView.dequeueReusableCell(withIdentifier: "LessonTriggerScheduleTableViewCell", for: indexPath) as! LessonTriggerScheduleTableViewCell
			cell.delegate = self
			//cell.isEnabled = contentMode == .editing
			return cell
		case 1:
			let cell = tableView.dequeueReusableCell(withIdentifier: "LessonTimeTableViewCell", for: indexPath) as! LessonTimeTableViewCell
			cell.delegate = self
			lesson?.lessonDate.bind(listener: { date in
				cell.dateTextField.text = date
			})
			lesson?.lessonTimeStart.bind(listener: { time in
				cell.timeTextField.text = time
			})
			lesson?.lessonDuration.bind(listener: { duration in
				cell.durationTextField.text = "\(duration / (1000 * 60))"
			})
			if contentMode == .creating {
				let formatter = DateFormatter()
				formatter.locale = Locale(identifier: "ru_RU")
				formatter.dateFormat = "dd MMMM yyyy"
				cell.dateTextField.text = formatter.string(from: pickedDate)
			}
			cell.isEnabled = contentMode == .creating
			return cell
		case 0:
			let cell = tableView.dequeueReusableCell(withIdentifier: "LessonTopTableViewCell", for: indexPath) as! LessonTopTableViewCell
			cell.delegate = self
			cell.group = group
			cell.pickedTopics = topics
			if let lesson = lesson {
				cell.setup(group: lesson.groupName.value, topics: lesson.lessonTopic.value.split(separator: ";").map({String($0)}), price: lesson.lessonPrice.value)
			}
			cell.isEnabled = contentMode == .creating
			return cell
		default:
			return UITableViewCell()
		}
	}
}

extension LessonViewController: LessonTopTableViewCellDelegate {
	func addStudentOrGroup() {
		let alert = UIAlertController(title: "Добавить группу или ученика", message: "", preferredStyle: .actionSheet)
		alert.addAction(UIAlertAction(title: "Добавить ученика", style: .default, handler: { _ in
			let controller = StudentDetailViewController.controller()
			controller.contentMode = .creating
			self.navigationController?.pushViewController(controller, animated: true)
		}))
		alert.addAction(UIAlertAction(title: "Добавить группу", style: .default) { _ in
			let controller = GroupDetailViewController.controller()
			controller.contentMode = .creating
			self.navigationController?.pushViewController(controller, animated: true)
		})
		alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: { _ in
			//
		}))
		present(alert, animated: true)
	}
	
	func selectGroup() {
		let controller = PickerViewController.controller()
		controller.pickerMode = .group
		controller.delegate = self
		let navController = UINavigationController(rootViewController: controller)
		present(navController, animated: true)
	}
	
	func topicPicked(_ topic: String) {
		self.topics.append(topic)
	}
	
	func topicRemoved(_ topic: String) {
		self.topics.removeAll { t in
			t == topic
		}
	}
	
	func priceSet(_ price: Int) {
		self.lessonPrice = price
	}
}

extension LessonViewController: LessonTriggerScheduleTableViewCellDelegate {
	func scheduleTriggered(_ isOn: Bool) {
		scheduleActive = isOn
	}
}

extension LessonViewController: LessonTimeTableViewCellDelegate {
	func datePicked(_ date: Date) {
		self.pickedDate = date
	}
	
	func timePicked(_ date: Date) {
		self.pickedTime = date
	}
	
	func durationPicked(_ duration: Int) {
		self.pickedDuration = duration * 60 * 1000
	}
}

extension LessonViewController: LessonScheduleTableViewCellDelegate {
	func endDatePicked(_ date: Date) {
		self.endDate = date
	}
	
	func scheduleTypeChanged(_ type: ScheduleType) {
		scheduleType = type
	}
	
	func pickWeekdays() {
		let controller = WeekdaysViewController.controller()
		controller.delegate = self
		let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: controller)
		bottomSheet.mdc_bottomSheetPresentationController?.preferredSheetHeight = 400
		present(bottomSheet, animated: true, completion: nil)
	}
}

extension LessonViewController: WeekdaysViewControllerDelegate {
	func weekdaysSelected(_ weekdays: [Weekday]) {
		self.selectedDays = weekdays
	}
}

extension LessonViewController: PickerViewControllerDelegate {
	func picker(_ controller: PickerViewController, didSelect value: Any?) {
		if let group = value as? Group {
			self.group = group
		}
	}
}
