//
//  TaskViewModel.swift
//  Tutorship
//
//  Created by Alexander on 10.06.2021.
//

import Foundation

class TaskViewModel {
	static func loadData(completion: @escaping ([TaskViewModel]) -> Void) {
		Repository.shared.getExpiredTasks { tasks in
			let viewModels = tasks.map { task in
				TaskViewModel(task: task)
			}
			completion(viewModels)
		}
	}
	private var task: ComplexTask!
	var taskID: LiveData<Int> = LiveData(0)
	var homeworkID: LiveData<Int> = LiveData(0)
	var passedAt: LiveData<String> = LiveData("")
	var isPassed: LiveData<Bool> = LiveData(false)
	var deadline: LiveData<String> = LiveData("")
	var studentID: LiveData<Int> = LiveData(0)
	var mark: LiveData<Int> = LiveData(0)
	var studentName: LiveData<String> = LiveData("")
	var lessonTopic: LiveData<String> = LiveData("")
	var comment: LiveData<String> = LiveData("")
	var homeworkContent: LiveData<String> = LiveData("")
	var createdAt: LiveData<String> = LiveData("")
	var taskStatus: LiveData<String> = LiveData("")
	
	init() {}
	
	init(task: ComplexTask) {
		self.task = task
		update()
	}
	
	private func update() {
		let dateFormatter = DateFormatter()
		dateFormatter.locale = Locale(identifier: "ru_RU")
		dateFormatter.dateFormat = "dd MMMM yyyy"
		
		taskID.value = task.id
		homeworkID.value = task.homeworkID
		passedAt.value = dateFormatter.string(from: task.passedAt)
		isPassed.value = task.isPassed
		deadline.value = dateFormatter.string(from: task.deadline)
		studentID.value = task.studentID
		mark.value = task.mark
		studentName.value = task.studentName
		lessonTopic.value = task.lessonTopic
		comment.value = task.comment
		homeworkContent.value = task.homeworkContent
		createdAt.value = dateFormatter.string(from: task.createdAt)
		
		if !task.isPassed {
			if task.deadline < Date() {
				taskStatus.value = "ПРОСРОЧЕНО"
			} else {
				taskStatus.value = "ЗАДАНО"
			}
		} else {
			if task.passedAt < task.deadline {
				taskStatus.value = "СДАНО"
			} else {
				taskStatus.value = "С ОПОЗДАНИЕМ"
			}
		}
	}
	
	func markAsPassed() {
		task.passedAt = Date()
		task.isPassed = true
		update()
		updateTask()
	}
	
	func updateTask(mark: Int, comment: String) {
		self.task.comment = comment
		self.task.mark = mark
		update()
		updateTask()
	}

	private func updateTask() {
		let task = Task(id: taskID.value, homeworkID: homeworkID.value, studentID: studentID.value, mark: self.task.mark, deadline: self.task.deadline, passedAt: self.task.passedAt, comment: self.task.comment)
		Repository.shared.updateTask(task: task) {
			Notifications.attendanceChanged.post()
		}
	}
}
