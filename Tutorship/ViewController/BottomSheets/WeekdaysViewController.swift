//
//  WeekdaysViewController.swift
//  Tutorship
//
//  Created by Alexander on 18.07.2021.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextFields

protocol WeekdaysViewControllerDelegate: AnyObject {
	func weekdaysSelected(_ weekdays: [Weekday])
}

class WeekdaysViewController: BaseViewController {
	
	override class func controller() -> Self {
		return WeekdaysViewController(nibName: String(describing: Self.self), bundle: nil) as! Self
	}

	@IBOutlet weak var tableView: UITableView!
	weak var delegate: WeekdaysViewControllerDelegate?
	private let weekdays: [Weekday] = [
		.monday,
		.tuesday,
		.wednesday,
		.thursday,
		.friday,
		.saturday,
		.sunday
	]
	var selectedDays: [Weekday] = [] {
		didSet {
			if isViewLoaded {
				tableView.reloadData()
			}
		}
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
		setupTableView()
    }
	
	private func setupTableView() {
		tableView.register(UINib(nibName: "WeekdayTableViewCell", bundle: nil), forCellReuseIdentifier: "WeekdayTableViewCell")
		tableView.dataSource = self
	}
	
	@IBAction func donePressed(_ sender: Any) {
		delegate?.weekdaysSelected(selectedDays)
		dismiss(animated: true, completion: nil)
	}
}

extension WeekdaysViewController: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		weekdays.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "WeekdayTableViewCell", for: indexPath) as! WeekdayTableViewCell
		cell.delegate = self
		cell.weekday = weekdays[indexPath.row]
		cell.weekdayLabel.text = weekdays[indexPath.row].rawValue
		cell.weekdaySwitch.isOn = selectedDays.contains(weekdays[indexPath.row])
		return cell
	}
}

extension WeekdaysViewController: WeekdayTableViewCellDelegate {
	func triggerWeekday(_ weekday: Weekday, isSelected: Bool) {
		if isSelected {
			selectedDays.append(weekday)
		} else {
			selectedDays.removeAll { w in
				w == weekday
			}
		}
	}
}

enum Weekday: String {
	case monday = "пн"
	case tuesday = "вт"
	case wednesday = "ср"
	case thursday = "чт"
	case friday = "пт"
	case saturday = "сб"
	case sunday = "вс"
	
	func getNumber() -> Int {
		switch self {
		case .monday:
			return 2
		case .tuesday:
			return 3
		case .wednesday:
			return 4
		case .thursday:
			return 5
		case .friday:
			return 6
		case .saturday:
			return 7
		case .sunday:
			return 1
		}
	}
}
