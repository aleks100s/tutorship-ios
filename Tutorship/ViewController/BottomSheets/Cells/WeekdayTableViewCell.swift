//
//  WeekdayTableViewCell.swift
//  Tutorship
//
//  Created by Alexander on 18.07.2021.
//

import UIKit

protocol WeekdayTableViewCellDelegate: AnyObject {
	func triggerWeekday(_ weekday: Weekday, isSelected: Bool)
}

class WeekdayTableViewCell: UITableViewCell {

	@IBOutlet weak var weekdaySwitch: UISwitch!
	@IBOutlet weak var weekdayLabel: UILabel!
	
	var weekday: Weekday?
	
	weak var delegate: WeekdayTableViewCellDelegate?
    
	@IBAction func switched(_ sender: UISwitch) {
		delegate?.triggerWeekday(weekday ?? .monday, isSelected: sender.isOn)
	}
}
