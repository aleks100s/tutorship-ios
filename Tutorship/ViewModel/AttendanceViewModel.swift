//
//  AttendanceViewModel.swift
//  Tutorship
//
//  Created by Alexander on 14.06.2021.
//

import Foundation

class AttendanceViewModel {
	static func getExpiredPayments(completion: @escaping ([AttendanceViewModel]) -> Void) {
		Repository.shared.getExpiredPayments { attendances in
			let viewModels = attendances.map { attendance in
				AttendanceViewModel(attendance: attendance)
			}
			completion(viewModels)
		}
	}
	
	static func getAttendances(for groupID: Int, lessonID: Int, completion: @escaping ([AttendanceViewModel]) -> Void) {
		Repository.shared.getAttendances(for: groupID, lessonID: lessonID) { attendances in
			let viewModels = attendances.map { attendance in
				AttendanceViewModel(attendance: attendance)
			}
			completion(viewModels)
		}
	}
	
	var attendanceID: LiveData<Int> = LiveData(0)
	var studentName: LiveData<String> = LiveData("")
	var studentPhone: LiveData<String> = LiveData("")
	var lessonDate: LiveData<String> = LiveData("")
	var lessonPrice: LiveData<String> = LiveData("₽")
	var lessonComment: LiveData<String> = LiveData("")
	var attendancePaid: LiveData<Bool> = LiveData(false)
	var studentID: LiveData<Int> = LiveData(0)
	var lessonID: LiveData<Int> = LiveData(0)
	var studentAbsent: LiveData<Bool> = LiveData(false)
	var studentMark: LiveData<Int> = LiveData(0)
	var attendanceComment: LiveData<String> = LiveData("")
	let initials: LiveData<String> = LiveData("")
	private var attendance: FullAttendance!
	
	init() {}
	
	init(attendance: FullAttendance) {
		self.attendance = attendance
		let dateFormatter = DateFormatter()
		dateFormatter.locale = Locale(identifier: "ru_RU")
		dateFormatter.dateFormat = "dd MMMM yyyy"
		
		attendanceID.value = attendance.attendance?.id ?? 0
		studentName.value = attendance.student?.fullName ?? ""
		studentPhone.value = attendance.student?.phone ?? ""
		lessonDate.value = dateFormatter.string(from: attendance.lesson?.datetime ?? Date())
		lessonPrice.value = "\(attendance.attendance?.price ?? 0)₽"
		lessonComment.value = attendance.lesson?.comment ?? ""
		attendancePaid.value = attendance.attendance?.isPaid ?? false
		
		let name = attendance.student?.fullName ?? ""
		let parts = name.split(separator: " ")
		if parts.count > 1 {
			initials.value = "\(parts[0].localizedCapitalized.first ?? " ")\(parts[1].localizedCapitalized.first ?? " ")"
		} else {
			if name.count > 1 {
				let index = name.index(name.startIndex, offsetBy: 1)
				initials.value = String(name[..<index])
			} else {
				initials.value = "\(name.first ?? " ")"
			}
		}
	}
	
	func updateAttendance(mark: Int, comment: String) {
		attendance.attendance?.mark = mark
		attendance.attendance?.comment = comment
		attendanceComment.value = comment
		studentMark.value = mark
		Repository.shared.updateAttendance(attendance: attendance.attendance!)
	}
	
	func markAsPaid() {
		let transaction = Transaction()
		transaction.amount = Double(attendance.attendance?.price ?? 0)
		transaction.attendanceID = attendance.attendance!.id
		transaction.timestamp = Date()
		Repository.shared.markAttendancePaid(transaction: transaction) { transaction in
			print(transaction.id)
			self.attendance.attendance?.isPaid = true
			self.attendancePaid.value = true
			Notifications.attendanceChanged.post()
		}
	}
}
