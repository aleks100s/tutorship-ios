//
//  FinishLessonRequest.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Foundation

class FinishLessonRequest: Encodable, BaseModel {
	internal init(attendances: [Attendance]) {
		self.attendances = attendances
	}
	
	var attendances: [Attendance]
}
