//
//  LessonsCountWithLastYearResponse.swift
//  Tutorship
//
//  Created by Alexander on 18.05.2021.
//

import Foundation

class LessonsCountWithLastYearResponse: Decodable, BaseModel {
	var lastYear: Int
	var thisYear: Int
	
	enum CodingKeys: String, CodingKey {
		case lastYear = "last_year"
		case thisYear = "this_year"
	}
}
