//
//  TaskViewController.swift
//  Tutorship
//
//  Created by Alexander on 16.06.2021.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class TaskViewController: BaseViewController {
	
	override class func controller() -> Self {
		return TaskViewController(nibName: String(describing: Self.self), bundle: nil) as! Self
	}

	@IBOutlet weak var studentNameLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!
	@IBOutlet weak var deadlineLabel: UILabel!
	@IBOutlet weak var statusLabel: UILabel!
	@IBOutlet weak var topicLabel: UILabel!
	@IBOutlet weak var contentLabel: UILabel!
	@IBOutlet weak var markTextField: MDCOutlinedTextField!
	@IBOutlet weak var commentTextField: MDCOutlinedTextField!
	@IBOutlet weak var button: UIButton!
	
	var task: TaskViewModel?
	
	override func viewDidLoad() {
        super.viewDidLoad()
		setupViews()
		displayTask()
    }
	
	override func viewWillDisappear(_ animated: Bool) {
		saveChanges()
	}
	
	private func setupViews() {
		UIHelper.prepareTextFields(textFields: [markTextField, commentTextField])
		markTextField.label.text = "Оценка за задание"
		commentTextField.label.text = "Комментарий"
	}
	
	private func displayTask() {
		task?.studentName.bind(listener: { value in
			self.studentNameLabel.text = value
		})
		task?.createdAt.bind(listener: { value in
			self.dateLabel.text = "Домашнее задание от \(value)"
		})
		task?.deadline.bind(listener: { value in
			self.deadlineLabel.text = "Дедлайн \(value)"
		})
		task?.taskStatus.bind(listener: { value in
			self.statusLabel.text = value
		})
		task?.lessonTopic.bind(listener: { value in
			self.topicLabel.text = value
		})
		task?.homeworkContent.bind(listener: { value in
			self.contentLabel.text = value
		})
		task?.mark.bind(listener: { value in
			self.markTextField.text = value == 0 ? "" : "\(value)"
		})
		task?.comment.bind(listener: { value in
			self.commentTextField.text = value
		})
		task?.isPassed.bind(listener: { isPassed in
			self.button.isHidden = isPassed
		})
	}
	
	private func saveChanges() {
		let mark = Int(markTextField.text ?? "0") ?? 0
		let comment = commentTextField.text ?? ""
		task?.updateTask(mark: mark, comment: comment)
	}

	@IBAction func markAsPassed(_ sender: Any) {
		task?.markAsPassed()
	}
}
