//
//  Lesson.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Foundation

class Lesson: Codable, BaseModel {
	var id: Int = 0
	var datetime: Date = Date()
	var duration: Double = 0
	var price: Int = 0
	var groupID: Int = 0
	var topic: String = ""
	var comment: String = ""
	var isFinished: Bool = false
	var tutor: Int = 0
	
	init(datetime: Date, duration: Double, price: Int, groupID: Int, topic: String, comment: String) {
		self.datetime = datetime
		self.duration = duration
		self.price = price
		self.groupID = groupID
		self.topic = topic
		self.comment = comment
	}
	
	required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		id  = try container.decode(Int.self, forKey: .id)
		let datetime = try container.decode(Double.self, forKey: .datetime)
		self.datetime = Date(timeIntervalSince1970: datetime / 1000)
		duration = try container.decode(Double.self, forKey: .duration)
		price = try container.decode(Int.self, forKey: .price)
		groupID = try container.decode(Int.self, forKey: .groupID)
		topic = try container.decode(String.self, forKey: .topic)
		comment = try container.decode(String.self, forKey: .comment)
		isFinished = try container.decode(Bool.self, forKey: .isFinished)
	}
	
	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(id, forKey: .id)
		try container.encode((datetime.timeIntervalSince1970 * 1000).rounded(), forKey: .datetime)
		try container.encode(duration, forKey: .duration)
		try container.encode(price, forKey: .price)
		try container.encode(comment, forKey: .comment)
		try container.encode(groupID, forKey: .groupID)
		try container.encode(topic, forKey: .topic)
		try container.encode(isFinished, forKey: .isFinished)
		try container.encode(Defaults.userID, forKey: .tutor)
	}
	
	enum CodingKeys: String, CodingKey {
		case id
		case datetime
		case duration
		case price
		case groupID = "group"
		case topic
		case comment
		case isFinished = "isFinished"
		case tutor
	}
}
