//
//  NetworkService.swift
//  Tutorship
//
//  Created by Alexander on 15.05.2021.
//

import Moya

typealias JSON = [String: Any]

enum NetworkService {
	case register(request: AuthRequest)
	case authorize(request: AuthRequest)
	case getProfileInfo
	case updateProfile(request: ProfileRequest)
	case getAllSubjects
	case createSubject(subject: Subject)
	case getSubjectTopics(subjectID: Int)
	case getAllStudents(includeArchived: Bool)
	case createStudent(student: Student)
	case getStudent(id: Int)
	case updateStudent(id: Int, student: Student)
	case deleteStudent(id: Int)
	case getAllGroups(includeIndividual: Bool)
	case createGroup(group: Group)
	case getGroup(id: Int)
	case updateGroup(id: Int, group: Group)
	case deleteGroup(id: Int)
	case createLinkAndProgress(link: Link)
	case getLinksForGroup(id: Int)
	case deleteLink(groupID: Int, studentID: Int)
	case createLesson(lesson: Lesson)
	case getLessons(dateFrom: Double, dateTo: Double, after: Double, groupID: Int)
	case getFullLesson(id: Int)
	case deleteLesson(id: Int)
	case updateLesson(id: Int, lesson: Lesson)
	case finishLesson(id: Int, request: FinishLessonRequest)
	case getStudentsOfGroup(id: Int)
	case getStudentDiscount(groupID: Int, studentID: Int)
	case getAttendances(lessonID: Int, groupID: Int)
	case updateAttendance(id: Int, attendance: Attendance)
	case payForLesson(transaction: Transaction)
	case createHomework(homework: Homework)
	case getHomework(groupID: Int, lessonID: Int)
	case lessonHasHomework(lessonID: Int)
	case getAttendance(id: Int)
	case getProgresses(groupID: Int)
	case getProgress(id: Int)
	case updateProgress(id: Int, progress: Progress)
	case getAttendanceFraction(progressID: Int)
	case getDebt(progressID: Int)
	case refillBalance(balanceRefill: BalanceRefill)
	case getTasks(groupID: Int)
	case getTask(id: Int)
	case updateTask(id: Int, task: Task)
	case getTransactions(last: Int)
	case getGroupTopics(groupID: Int)
	case getExpiredTasks
	case getDebtors
	case getAttendancePercent(start: Double, end: Double, groupID: Int)
	case getTotalIncome(start: Double, end: Double, groupID: Int)
	case getLessonsCount(start: Double, end: Double, groupID: Int)
	case getTotalIncomeWithLastYear(start: Double, end: Double)
	case getLessonsCountWithLastYear(start: Double, end: Double)
	case postFeedback(request: FeedbackRequest)
	case getNotifications
	case deleteNotification(id: Int)
	case getTotalIncomeExport(start: Double, end: Double, groupID: Int)
	case getLessonsCountExport(start: Double, end: Double, groupID: Int)
}

extension NetworkService: TargetType {
	var baseURL: URL {
		URL(string: "http://207.154.251.171/api/")!
		//URL(string: "http://127.0.0.1:8000/api/")!
	}

	var path: String {
		switch self {
		case .register(_):
			return "register/"
		case .authorize(_):
			return "auth/"
		case .getProfileInfo:
			return "self/"
		case .updateProfile(_):
			return "self/update"
		case .getAllSubjects:
			return "subjects/"
		case .createSubject(_):
			return "subjects/"
		case .getSubjectTopics(let id):
			return "subject/\(id)/topics/"
		case .getAllStudents(_), .createStudent(_):
			return "students/"
		case .getStudent(let id), .updateStudent(let id, _), .deleteStudent(let id):
			return "student/\(id)/"
		case .getAllGroups:
			return "groups/"
		case .createGroup(_):
			return "groups/"
		case .getGroup(let id), .updateGroup(let id, _), .deleteGroup(let id):
			return "group/\(id)/"
		case .createLinkAndProgress(_), .getLinksForGroup(_):
			return "links/"
		case .deleteLink(let groupID, let studentID):
			return "group/\(groupID)/students/\(studentID)/remove"
		case .createLesson(_):
			return "lessons/"
		case .getLessons(_, _, _, _), .getFullLesson(_):
			return "full_lessons/"
		case .deleteLesson(let id), .updateLesson(let id, _):
			return "lesson/\(id)/"
		case .finishLesson(let id, _):
			return "lesson/\(id)/finish/"
		case .getStudentsOfGroup(let id):
			return "group/\(id)/students/"
		case .getStudentDiscount(let groupID, let studentID):
			return "group/\(groupID)/student/\(studentID)/"
		case .getAttendances(_, _):
			return "attendances/"
		case .updateAttendance(let id, _):
			return "attendances/\(id)/update/"
		case .payForLesson(_):
			return "transactions/"
		case .createHomework(_):
			return "homeworks/"
		case .getHomework(_, _):
			return "homeworks/full/"
		case .lessonHasHomework(let id):
			return "lesson/\(id)/has_homework"
		case .getAttendance(let id):
			return "attendance/\(id)/"
		case .getProgresses(_):
			return "progress/"
		case .getProgress(let id), .updateProgress(let id, _):
			return "progress/\(id)/"
		case .getAttendanceFraction(let id):
			return "progress/\(id)/attendance_fraction/"
		case .getDebt(let id):
			return "progress/\(id)/debt/"
		case .refillBalance(_):
			return "progress/refill_balance/"
		case .getTasks(_):
			return "tasks/"
		case .getTask(let id), .updateTask(let id, _):
			return "tasks/\(id)/"
		case .getTransactions(_):
			return "transactions"
		case .getGroupTopics(let id):
			return "group/\(id)/topics/"
		case .getExpiredTasks:
			return "tasks/expired/"
		case .getDebtors:
			return "attendances/debtors/"
		case .getAttendancePercent(_, _, _):
			return "statistics/attendance_percent"
		case .getTotalIncome(_, _, _):
			return "statistics/total_income"
		case .getLessonsCount(_, _, _):
			return "statistics/lessons_count"
		case .getTotalIncomeWithLastYear(_, _):
			return "statistics/total_income/with_last_year"
		case .getLessonsCountWithLastYear(_, _):
			return "statistics/lessons_count/with_last_year"
		case .postFeedback(_):
			return "feedback/"
		case .getNotifications:
			return "notifications/"
		case .deleteNotification(let id):
			return "notifications/\(id)/"
		case .getTotalIncomeExport(_, _, _):
			return "statistics/total_income/export"
		case .getLessonsCountExport(_, _, _):
			return "statistics/lessons_count/export"

		}
	}
	
	var method: Method {
		switch self {
		case .register(_), .authorize(_), .createSubject(_), .createStudent(_), .createGroup(_), .createLinkAndProgress(_), .createLesson(_), .finishLesson(_, _), .payForLesson(_), .createHomework(_), .refillBalance(_), .postFeedback(_):
			return .post
		case .getProfileInfo, .getAllSubjects, .getSubjectTopics(_), .getAllStudents(_), .getStudent(_), .getAllGroups(_), .getLinksForGroup(_), .getLessons(_, _, _, _), .getFullLesson(_), .getStudentsOfGroup(_), .getStudentDiscount(_, _), .getAttendances(_, _), .getHomework(_, _), .lessonHasHomework(_), .getAttendance(_), .getProgress(_), .getProgresses(_), .getAttendanceFraction(_), .getDebt(_), .getTask(_), .getTasks(_), .getTransactions(_), .getGroupTopics(_), .getExpiredTasks, .getDebtors, .getAttendancePercent(_, _, _), .getTotalIncome(_, _, _), .getLessonsCount(_, _, _), .getTotalIncomeWithLastYear(_, _), .getLessonsCountWithLastYear(_, _), .getNotifications, .getTotalIncomeExport(_, _, _), .getLessonsCountExport(_, _, _), .getGroup(_):
			return .get
		case .updateProfile(_), .updateStudent(_, _), .updateGroup(_, _), .updateLesson(_, _), .updateAttendance(_, _), .updateProgress(_, _), .updateTask(_, _):
			return .put
		case .deleteStudent(_), .deleteGroup(_), .deleteLink(_, _), .deleteLesson(_), .deleteNotification(_):
			return .delete
		}
	}
	
	var sampleData: Data {
		"".utf8Encoded
	}
	
	var task: Moya.Task {
		switch self {
		case .getProfileInfo, .getAllSubjects, .getExpiredTasks, .getDebtors, .getNotifications, .getSubjectTopics(_), .getStudent(_), .getStudentsOfGroup(_), .getStudentDiscount(_, _), .lessonHasHomework(_), .getAttendance(_), .getProgress(_), .getAttendanceFraction(_), .getDebt(_), .getTask(_), .getGroupTopics(_), .deleteStudent(_), .deleteGroup(_), .deleteLink(_, _), .deleteLesson(_), .deleteNotification(_), .getGroup(_):
			return .requestPlain
		case .register(let request), .authorize(let request):
			return .requestJSONEncodable(request)
		case .createSubject(let subject):
			return .requestJSONEncodable(subject)
		case .createStudent(let student), .updateStudent(_, let student):
			return .requestJSONEncodable(student)
		case .createGroup(let group), .updateGroup(_, let group):
			return .requestJSONEncodable(group)
		case .createLinkAndProgress(let link):
			return .requestJSONEncodable(link)
		case .createLesson(let lesson), .updateLesson(_, let lesson):
			return .requestJSONEncodable(lesson)
		case .finishLesson(_, let request):
			return .requestJSONEncodable(request)
		case .payForLesson(let transaction):
			return .requestJSONEncodable(transaction)
		case .getAllStudents(let includeArchived):
			return .requestParameters(parameters: ["include_archived": includeArchived], encoding: URLEncoding.default)
		case .getAllGroups(let includeIndividual):
			return .requestParameters(parameters: ["include_individual": includeIndividual], encoding: URLEncoding.default)
		case .getLinksForGroup(let groupID), .getProgresses(let groupID), .getTasks(let groupID):
			return .requestParameters(parameters: ["group": groupID], encoding: URLEncoding.default)
		case .getLessons(let dateFrom, let dateTo, let after, let groupID):
			return .requestParameters(parameters: ["from": dateFrom, "to": dateTo, "after": after, "group": groupID], encoding: URLEncoding.default)
		case .getFullLesson(let id):
			return .requestParameters(parameters: ["lesson": id], encoding: URLEncoding.default)
		case .getAttendances(let lessonID, let groupID), .getHomework(let groupID, let lessonID):
			return .requestParameters(parameters: ["lesson": lessonID, "group": groupID], encoding: URLEncoding.default)
		case .getTransactions(let last):
			return .requestParameters(parameters: ["last": last], encoding: URLEncoding.default)
		case .getAttendancePercent(let start, let end, let groupID), .getTotalIncome(let start, let end, let groupID), .getLessonsCount(let start, let end, let groupID), .getTotalIncomeExport(let start, let end, let groupID), .getLessonsCountExport(let start, let end, let groupID):
			return .requestParameters(parameters: ["start": start, "end": end, "group": groupID], encoding: URLEncoding.default)
		case .getTotalIncomeWithLastYear(let start, let end), .getLessonsCountWithLastYear(let start, let end):
			return .requestParameters(parameters: ["start": start, "end": end], encoding: URLEncoding.default)
		case .postFeedback(let request):
			return .requestJSONEncodable(request)
		case .updateProfile(let request):
			return .requestJSONEncodable(request)
		case .updateAttendance(_, let attendance):
			return .requestJSONEncodable(attendance)
		case .updateProgress(_, let progress):
			return .requestJSONEncodable(progress)
		case .updateTask(_, let task):
			return .requestJSONEncodable(task)
		case .createHomework(let homework):
			return .requestJSONEncodable(homework)
		case .refillBalance(let balanceRefill):
			return .requestJSONEncodable(balanceRefill)
		}
	}
	
	var headers: [String : String]? {
		[
			"Content-type": "application/json",
			"Authorization": "Token \(Defaults.token ?? "")"
		]
	}
}
// MARK: - Helpers
private extension String {
	var urlEscaped: String {
		return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
	}

	var utf8Encoded: Data {
		return data(using: .utf8)!
	}
}
