//
//  TransactionTableViewCell.swift
//  Tutorship
//
//  Created by Alexander on 03.06.2021.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {

	@IBOutlet weak var idLabel: UILabel!
	@IBOutlet weak var sumLabel: UILabel!
	@IBOutlet weak var lessonLabel: UILabel!
	@IBOutlet weak var studentLabel: UILabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
	
	func setupCell(with transaction: TransactionViewModel) {
		transaction.transactionID.bind { id in
			self.idLabel.text = "\(id)"
		}
		transaction.transactionAmount.bind { amount in
			self.sumLabel.text = amount
		}
		transaction.transactionStudent.bind { student in
			self.studentLabel.text = student
		}
		transaction.transactionLessonID.bind { id in
			self.lessonLabel.text = "\(id)"
		}
		studentLabel.textColor = Colors.accent
	}
    
}
