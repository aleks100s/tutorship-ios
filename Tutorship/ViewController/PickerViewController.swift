//
//  PickerViewController.swift
//  Tutorship
//
//  Created by Alexander on 01.08.2021.
//

import UIKit

protocol PickerViewControllerDelegate: AnyObject {
	func picker(_ controller: PickerViewController, didSelect value: Any?)
	
	func picker(_ controller: PickerViewController, didSelect values: [Any])
	
	func picker(_ controller: PickerViewController, didCancelSelection: Void)
}

extension PickerViewControllerDelegate {
	func picker(_ controller: PickerViewController, didSelect value: Any?) {}
	
	func picker(_ controller: PickerViewController, didSelect values: [Any]) {}
	
	func picker(_ controller: PickerViewController, didCancelSelection: Void) {}
}

class PickerViewController: UIViewController {
	
	class func controller() -> Self {
		UIStoryboard(name: "Picker", bundle: .main).instantiateViewController(withIdentifier: String(describing: Self.self)) as! Self
	}

	@IBOutlet weak var searchBar: UISearchBar!
	@IBOutlet weak var tableView: UITableView!
	
	private var filteredValues: [Any] = [] {
		didSet {
			tableView.indexPathsForSelectedRows?.forEach({ indexPath in
				tableView.deselectRow(at: indexPath, animated: true)
			})
			tableView.reloadData()
		}
	}
	private var values: [Any] = [] {
		didSet {
			filteredValues = values
		}
	}
 
	weak var delegate: PickerViewControllerDelegate?
	var pickerMode: PickerMode = .subject
	var multipleSelection: Bool = false
	var subjectID: Int = 0
	
	override func viewDidLoad() {
        super.viewDidLoad()
		setupSelf()
        setupTableView()
		loadValues()
    }
	
	private func setupSelf() {
		title = pickerMode.rawValue
		searchBar.delegate = self
	}
	
	private func setupTableView() {
		tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
		tableView.dataSource = self
		tableView.delegate = self
		tableView.allowsMultipleSelection = multipleSelection
	}
	
	private func loadValues() {
		switch pickerMode {
		case .group:
			loadGroups()
		case .subject:
			loadSubjects()
		case .topic:
			loadTopics()
		}
	}
	
	private func loadSubjects() {
		Repository.shared.getSubjectsData { subjects in
			self.values = subjects
		}
	}
	
	private func loadGroups() {
		Repository.shared.getAllGroups { groups in
			self.values = groups
		}
	}
	
	private func loadTopics() {
		Repository.shared.getTopics(subjectID: subjectID) { topics in
			self.values = topics
		}
	}
	
	@IBAction func donePresed(_ sender: Any) {
		if multipleSelection {
			let selectedValues = tableView.indexPathsForSelectedRows?.map({$0.row}).map({self.filteredValues[$0]}) ?? []
			delegate?.picker(self, didSelect: selectedValues)
		} else {
			var selectedValue: Any? = nil
			if tableView.indexPathForSelectedRow != nil {
				selectedValue = filteredValues[tableView.indexPathForSelectedRow!.row]
			}
			delegate?.picker(self, didSelect: selectedValue)
		}
		navigationController?.dismiss(animated: true)
	}
	
	@IBAction func cancelPressed(_ sender: Any) {
		delegate?.picker(self, didCancelSelection: Void())
		navigationController?.dismiss(animated: true)
	}
}

extension PickerViewController: UISearchBarDelegate {
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		if searchText.isEmpty {
			filteredValues = values
			return
		}
		switch pickerMode {
		case .subject:
			filteredValues = values.filter({ value in
				if let val = value as? Subject {
					return val.name.lowercased().contains(searchText.lowercased())
				}
				return false
			})
		case .group:
			filteredValues = values.filter({ value in
				if let val = value as? Group {
					return val.name.lowercased().contains(searchText.lowercased())
				}
				return false
			})
		case .topic:
			filteredValues = values.filter({ value in
				if let val = value as? String {
					return val.lowercased().contains(searchText.lowercased())
				}
				return false
			})
		}
	}
}

extension PickerViewController: UITableViewDataSource, UITableViewDelegate {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		filteredValues.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
		cell.backgroundColor = Colors.primaryDark
		cell.textLabel?.textColor = .white
		cell.detailTextLabel?.textColor = Colors.accent
		switch pickerMode {
		case .group:
			if let group = filteredValues[indexPath.row] as? Group {
				cell.textLabel?.text = group.name
			}
		case .subject:
			if let subject = filteredValues[indexPath.row] as? Subject {
				cell.textLabel?.text = subject.name
			}
		case .topic:
			if let topic = filteredValues[indexPath.row] as? String {
				cell.textLabel?.text = topic
			}
		}
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let cell = tableView.cellForRow(at: indexPath)
		cell?.accessoryType = .checkmark
	}
	
	func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
		let cell = tableView.cellForRow(at: indexPath)
		cell?.accessoryType = .none
	}
}

enum PickerMode: String {
	case subject = "Предмет"
	case group = "Группа или ученик"
	case topic = "Тема урока"
}
